<?php 
	include 'head.php';
	include 'acessoRestrito.php'
?>
	<title>Smell of Love | <?php echo $lang["cupons de desconto"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php include 'headerAdm.php'; ?>
		<section>
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row mx-2">
					<div class="col-12">
						<h2 class="text-center my-3"><?php echo $lang['cadastrar novo cupom']; ?></h2>
						<table class="table table-bordered my-2 table-responsive">
							<thead class="thead-inverse">
								<tr>
									<th class="w-50 text-center"><?php echo $lang["Código"]; ?></th>
									<th class="w-25 text-center"><?php echo $lang["Valor Fixo"]; ?></th>
									<th class="w-75 text-center"><?php echo $lang["Valor Percentual"]; ?></th>
									<th class="w-25 text-center"><?php echo $lang["Cadastrar"]; ?></th>
								</tr>
							</thead>
							<tbody>
								<tr class="fonte-normal">
									<form action="novoCupom.php" method="POST">
										<td class="align-middle text-center text-sm-left">								
											<input id="novoCupomCodigo" name="novoCupomCodigo" type="text" class="form-control rounded-0 fonte-normal w-100" required="required" onblur="validar(this)">
											<small class="fonte-normal hidden-sm-down"><?php echo $lang["O código deve conter 8 números"]; ?> </small>
										</td>
										<td class="align-middle text-center text-sm-left">
											<div class="form-check form-check-inline">
												<label for="valorFixo" class="form-check-label fonte-normal custom-control custom-radio mr-0 w-25">
													<input type="radio" class="form-check-input custom-control-input" name="tipoValor" id="valorFixo" value="F" required="required" onclick="bloquearValorOposto(this)">
													<span class="custom-control-indicator"></span>
												</label>
											</div>										
											<input id="novoCupomFixo" name="novoCupomFixo" type="text" class="fonte-normal w-75">
										</td>
										<td class="align-middle text-center text-sm-left">
											<div class="form-check form-check-inline">
												<label for="valorPorcentagem" class="form-check-label fonte-normal custom-control custom-radio mr-0 w-25">
													<input type="radio" class="form-check-input custom-control-input" name="tipoValor" id="valorPorcentagem" value="P" onclick="bloquearValorOposto(this)">
													<span class="custom-control-indicator"></span>
												</label>
											</div>	
											<input id="novoCupomPercentual" name="novoCupomPercentual" type="number" min="1" max="100" class="fonte-normal w-75">
										</td>
										<td class="align-middle text-center">
											<button id="submitNovoCupom" type="submit" class='btn btn-sm btn-success' disabled="disabled">
												<span class='fa fa-plus'></span>
											</button>
										</td>
									</form>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="row mx-2">
					<div class="col-12">
						<h2 class="text-center my-3"><?php echo $lang["Cupons cadastrados"]; ?></h2>
						<table class="table table-striped table-bordered table-hover my-2 table-responsive">
							<thead class="thead-inverse">
								<tr>
									<th class="w-25">
										<?php echo $lang["Código"]; ?>
									</th>
									<th class="w-50">
										<?php echo $lang["Descrição"]; ?>
									</th>
									<th class="w-25" class="text-center">
										<?php echo $lang["Deletar"]; ?>
									</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$sql = "SELECT * FROM cupons";
									$res = $con->query($sql);

									if ($res) {
										while ($linha = $res->fetch_assoc()) { 											
											echo "	<tr classs='fonte-normal'>
														<td name='codigo[]'> {$linha["codigo"]} </td>
														<td class='w-75'>
															<span class='fonte-normal'>{$linha[$lang['descricao']]}</span>
														</td>
														<td class='text-center'>
															<button value='{$linha["codigo"]}' onclick='deletarCupom(this)' class='btn btn-sm btn-danger'>
																<span class='fa fa-remove'></span>
															</button>
														</td>
													</tr>";
										}
									}
									mysqli_close($con);
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</section>
		<?php include 'footer.php'; ?>
</html>