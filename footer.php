	<footer id="footer" class="footer-site">
		<div class="col-md-12 pt-1">
			<div class="row clearfix justify-content-center justify-content-sm-start">
				<div class="col-6 col-sm-4 col-md-2 offset-md-2 justify-content-center pb-4">
					<p id="redes" class="text-center m-0 p-0 footer-titulo"><?php echo $lang["Nossas redes sociais"]; ?>:</p>
					<div class="row justify-content-center icones">
							<a target="_blank" href="<?php echo $linkFacebookSite; ?>" style="color:white" class="btn btn-sm btn-link px-2 py-1 face mt-1"><span class="fa fa-facebook-f"></span></a>
							<a target="_blank" href="<?php echo $linkInstagramSite; ?>" style="color:white" class="btn btn-sm btn-link px-2 py-1 insta mt-1"><span class="fa fa-instagram"></span></a>
							<a target="_blank" href="<?php echo $linkSnapchatSite; ?>" style="color:white" class="btn btn-sm btn-link px-2 py-1 snap mt-1"><span class="fa fa-snapchat-ghost"></span></a>
					</div>
				</div>
				<div class="col-7 col-sm-4 col-md-4 pb-4 pb-sm-1">
					<p id="fale" class="text-center p-0 m-0 footer-titulo flex-last"><?php echo $lang["Fale conosco"]; ?>:</p>
					<ul class="list-group fale">
						<li class="list-group-item item-fale fonte-normal justify-content-center p-0 m-1"><p class="text-center fonte-normal"><i class="fa fa-map-marker"></i><br><?php echo "$ruaSite, {$lang['nº']} $numeroSite <br> $bairroSite <br> $cepSite <br> $cidadeSite"; ?></p></li>
						<li class="list-group-item item-fale fonte-normal justify-content-center p-0 m-1"><p class="text-center fonte-normal"><i class="fa fa-mobile-phone"></i><?php echo "<br> $telefoneSite <br> $telefone2Site"; ?></p></li>
						<li class="list-group-item item-fale fonte-normal justify-content-center p-0 m-1"><p class="text-center fonte-normal mb-0"><i class="fa fa-envelope"></i><br><?php echo "$emailSite <br> $email2Site"; ?></p></li>
					</ul>
				</div>
				<div class="col-7 col-sm-4 col-md-2 pb-4">
					<p id="formas" class="text-center footer-titulo p-0 m-0"><?php echo $lang["Formas de pagamento"]; ?>:</p>
					<ul class="row justify-content-center list-inline">
						<li class="list-inline-item"><i class="fa pagamento fa-cc-visa p-0 m-1"></i></li>
						<li class="list-inline-item"><i class="fa pagamento fa-cc-amex p-0 m-1"></i></li>
						<li class="list-inline-item"><i class="fa pagamento fa-credit-card p-0 m-1"></i></li>
						<li class="list-inline-item"><i class="fa pagamento fa-cc-paypal p-0 m-1"></i></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="row clearfix">
				<div class="col-md-12 direitos">
					<p class="text-center m-0 p-0 fonte-normal copy">&copy; 2017 - Smell of Love - <?php echo $lang["Todos os direitos reservados"]; ?></p>
				</div>
			</div>
		</div>
	</footer>
	<script src="_assets/_scripts/jquery.min.js"></script>
    <script src="_assets/tether/dist/js/tether.min.js"></script>
    <script src="_assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="_assets/jQueryMask/jquery.mask.min.js"></script>
    <script src="_assets/viaCEP/viaCEP.js"></script>    
	<script type="text/javascript" src="_assets/_scripts/script.js"></script>
	<script type="text/javascript" src="_assets/_scripts/ajax.js"></script>
</body>