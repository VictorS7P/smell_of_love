<?php 
	include 'head.php';
?>
	<title>Smell of Love | <?php echo $lang["Sobre"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			setarHeader();
			$sql = "SELECT * FROM sobre";
			$res = $con->query($sql);

			if ($res) {
				while ($linha = $res->fetch_assoc()) {
		?>
		<section>
			<div class="col-12 col-md-8 offset-md-2 sobre">
				<div class="row align-items-center mt-3">
					<div class="col-12">
						<div class="jumbotron py-3 rounded-0 item-sobre">
							<h2 class="display-4"><?php echo $lang["História"]; ?></h2>
							<hr class="jumbotron-hr">
							<p class="fonte-normal lead">
								<?php echo $linha[$lang["historia"]] ?>
							</p>
						</div>
						</div>
					</div>
				<div class="row align-items-cente">
					<div class="col-12">
						<div class="jumbotron py-3 rounded-0 item-sobre">
							<h2 class="display-4"><?php echo $lang["Missão"]; ?></h2>
							<hr class="jumbotron-hr">
							<p class="fonte-normal lead">
								<?php echo $linha[$lang["missao"]] ?>
							</p>
						</div>
					</div>
				</div>

				<div class="row align-items-center">
					<div class="col-12">
						<div class="jumbotron py-3 rounded-0 item-sobre">
							<h2 class="display-4"><?php echo $lang["Visão"]; ?></h2>
							<hr class="jumbotron-hr">
							<p class="fonte-normal lead">
								<?php echo $linha[$lang["visao"]] ?>
							</p>
						</div>
					</div>
				</div>

				<div class="row align-items-center">
					<div class="col-12">
						<div class="jumbotron py-3 rounded-0 item-sobre">
							<h2 class="display-4"><?php echo $lang["Valores"]; ?></h2>
							<hr class="jumbotron-hr">
							<p class="fonte-normal lead">
								<?php echo $linha[$lang["valores"]] ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php 
				}
			}
			include 'footer.php'; ?>
</html>