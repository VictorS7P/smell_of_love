<?php
	if (isset($_COOKIE['idioma'])) {
		if ($_COOKIE['idioma'] == "en") {
			include 'en.php';
		} else if ($_COOKIE['idioma'] == "dn") {
			include 'dn.php';
		}
	} else {
		include 'pt-br.php';
	}
?>

<header>
			<div id="barraHeader" class="col-md-12 col-sm-12 col-12">
				<div class="row barra-topo nodropped rounded-0 align-items-center">
					<div id="idiomas" class="col-md-2 col-sm-12 col-12 pl-2 pr-0 align-self-baseline flex-md-last">
						<div class="row justify-content-center idiomas">
							<ul class="list-inline d-inline">
								<li class="list-inline-item">
									<button class="p-0 m-0 sem-fundo" onclick="traduzir('pt')">
										<img src="_assets/imagens/idiomas/pt.png" alt="Pt-Br">
									</button>
								</li>								

								<li class="list-inline-item">
									<button class="p-0 m-0 sem-fundo" onclick="traduzir('en')">
										<img src="_assets/imagens/idiomas/in.png" alt="En">
									</button>
								</li>

								<li class="list-inline-item">
									<button class="p-0 m-0 sem-fundo" onclick="traduzir('dn')">
										<img src="_assets/imagens/idiomas/dn.png" alt="Dn">
									</button>
								</li>								
							</ul>				
						</div>
					</div>

					<div id="barraLogin" class="col-md-5 push-1 col-sm-12 col-12 btn-topo no-gutters pr-0 pl-0 align-self-baseline mb-1 mb-md-0">
						<div class="row no-gutters justify-content-center">
							<a href="perfil.php" class="btn botao-user btn-sm btn-default rounded-0 px-sm-0 pb-md-2 col-2 mx-1">
								<span class="fonte-normal"><i class="fa fa-user" aria-hidden="true"></i> <?php echo $lang["Perfil"]; ?></span>
							</a>
							<span class="fonte-normal text-center mx-1">
								<small class="fonte-normal">
									<?php echo $lang["Bem-vindo"]; ?> <?php echo $_SESSION['nome']; ?>
								</small>
							</span>													
							<a href="logout.php" class="btn botao-user btn-sm btn-default rounded-0 px-sm-0 pb-md-2 col-2 mx-1">
								<span class="fonte-normal"><i class="fa fa-sign-out" aria-hidden="true"></i> <?php echo $lang["Sair"]; ?></span>
							</a>
						</div>
					</div>


					<div id="barraCarrinho" class="col-12 col-sm-12 col-md-4 offset-md-1 pr-md-0 mr-md-0 pl-md-0 pt-md-0">
						<?php 
							if ($_SESSION['tipo'] == 2) {
								echo "
										<div class='acessar-painel'>
											<a href='administrador.php' class='btn btn-padrao acessar-painel-a'>Painel<br>Administrativo</a>
										</div>
								";
							} else {
						?>
						<div id="linhaCarrinho" class="row justify-content-md-center">
							<div id="colCarrinho" class="col-6 col-sm-6 col-md-5 px-0 px-sm-0 carrinho">
								<span class="float-right">
									<i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;<?php echo $lang["Meu carrinho"]; ?>:&nbsp;
									<span id="itens" class="ml-0 mr-0 pr-2 pb-1">
										<?php
											if (isset($_COOKIE['carrinho'])) {
													$carrinho = unserialize($_COOKIE['carrinho']);
													$tamanho = count($carrinho);
													echo $tamanho;
											} else {
												echo "0";
											}
										?>
									</span>
								</span>
							</div>

							<div id="colCarrinho2" class="col-6 col-sm-6 col-md-5 px-0 px-sm-0 carrinho">		
								<span id="valorCarrinho" class="float-left float-md-right">									
									<a href="checkout.php" class="btn btn-sm btn-success rounded-0 p-1 btn-comprar">
									<p class="p-0 m-0"><i class="fa fa-usd" aria-hidden="true"></i> <?php echo $lang["Finalizar"]; ?></p>
									</a>
								</span>
							</div>
						</div>
						<?php } ?>
					</div>		
				</div>
			</div>
			<div class="col-12">
				<div class="row clearfix logo-pesquisa">
					<div class="col-md-3 offset-md-2 logo pt-1 pb-1 col-sm-12 offset-sm-0 col-12 offset-12">
						<a href="index.php">
							<img src="_assets/imagens/logo-b.png" class="img-fluid" alt="Smell of Love" />
						</a>
					</div>

					<div class="col-md-4 offset-md-1 align-self-center pr-md-4 pl-md-5 col-11 pb-1 pt-1 pt-md-0 pb-md-0">
						<span class="form-control form-inline margin-0 p-0 pl-1">
							<input type="text" id="pesquisa" name="pesquisa" placeholder="<?php echo $lang["Pesquisar"]; ?>..." class="fonte-normal">	
							<button value="pesquisar" class="btn btn-sm btn-default pl-0" onclick="pesquisar()">
								<i class="fa fa-search" aria-hidden="true" class=""></i>
							</button>
						</span>
					</div>

					<div class="col-1 offset-md-1 align-self-center">
					<button id="mostrar" value="header" class="btn-header btn btn-sm rounded-0 border-0 float-right">
							<i class="fa fa-chevron-down" aria-hidden="true"></i>
					</button>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="row barra-links">
					<div class="col-12 col-md-3 offset-md-2 pr-0 pl-0">
						<nav>
							<ul class="nav justify-content-around">
								<li class="nav-item px-0">
									<a class="nav-link text-center link-site" href="sobre.php">
										<?php echo $lang["Sobre"]; ?>
									</a>
								</li>
								<li class="nav-item px-0">
									<a class="nav-link text-center link-site" href="contato.php">
										<?php echo $lang["Contato"]; ?>
									</a>
								</li>
							</ul>
						</nav>
					</div>
					<div class="col-12 col-md-4 offset-md-1 pr-0 pl-0 barra-produtos">
						<nav>
							<ul class="nav justify-content-around ml-md-5 mr-md-4">
								<li class="nav-item px-0">
									<a href="feminino.php" class="nav-link text-center link-site">
										<?php echo $lang["Feminino"]; ?>
									</a>
								</li>
								<li class="nav-item px-0">
									<a href="masculino.php" class="nav-link text-center link-site">
										<?php echo $lang["Masculino"]; ?>
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>
  </div>
</div>