<?php
	include "head.php";
	include "acessoRestrito.php";

	$sql = "SELECT * FROM mensagens WHERE checada = 'N';";

	$res = $con->query($sql);
?>
	<title>Smell of Love | <?php echo $lang["Mensagens"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php include 'headerAdm.php'; ?>
		<section>
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row mx-2 my-3 justify-content-center">
					<?php
						if ($res) {
							while ($linha = $res->fetch_assoc()) {
								echo
									"
									<div class='col-12 col-sm-6 my-1 px-1' id='mensagem-{$linha['id']}'>
									<div class='card'>
										<div class='card-block'>
											<h3 class='card-title fonte-normal mb-0'>{$linha['nome']}</h3>
											<p class='card-text mb-1'>
												<small class='fonte-normal text-muted'>
													{$lang["e-mail"]}: <span id='email-{$linha['id']}' class='fonte-normal text-muted'>{$linha['email']}</span> | 
													{$lang["Telefone"]}: {$linha['telefone']}
												</small>
											</p>
											<p class='cerd-text lead fonte-normal text'>
												{$linha['mensagem']}
											</p>
											<p class='text-center mb-0'>
												<button onclick='arquivarMensagem(this)' value='{$linha['id']}' class='btn btn-sm btn-padrao'>{$lang["Arquivar"]}</button>
												<button onclick='responderMensagem(this)' value='{$linha['id']}' class='btn btn-sm btn-success'>{$lang["Responder"]}</button>
												<button onclick='excluirMensagem(this)' value='{$linha['id']}' class='btn btn-sm btn-danger'>{$lang["Excluir"]}</button>
											</p>
										</div>
									</div>
									</div>";
							}
						}
					?>
				</div>
				<div class="row justify-content-center">
					<div class="col-12">
						<button class="py-3 my-0 btn btn-padrao btn-block my-2" data-toggle="modal" data-target="#arquivadas">Arquivadas</button>
					</div>
				</div>
			</div>

			<div id="arquivadas" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header justify-content-center">
							<div class="modal-title titulo-modal text-center">
								<h2>
									<?php echo $lang["Mensagens Arquivadas"]; ?>									
								</h2>
								<a href="admMensagens.php" class="btn-link page-link"><small class="text-center fonte-normal"><?php echo $lang["Recarregue a página para atualizar as mensagens"]; ?></small></a>
							</div>
						</div>
						<div class="modal-body">
							<div class="row">
							<?php
								$sql = "SELECT * FROM mensagens WHERE checada = 'S';";
								$res = $con->query($sql);
								if ($res) {
									while ($linha = $res->fetch_assoc()) {
										echo
											"
											<div class='col-12 col-sm-6 my-1 px-1' id='mensagem-{$linha['id']}'>
											<div class='card'>
												<div class='card-block'>
													<h3 class='card-title fonte-normal mb-0'>{$linha['nome']}</h3>
													<p class='card-text mb-1'>
														<small class='fonte-normal text-muted'>
															{$lang["e-mail"]}: <span id='email-{$linha['id']}' class='fonte-normal text-muted'>{$linha['email']}</span> | 
															{$lang["Telefone"]}: {$linha['telefone']}
														</small>
													</p>
													<p class='cerd-text lead fonte-normal text'>
														{$linha['mensagem']}
													</p>
													<p class='text-center mb-0'>
														<button onclick='responderMensagem(this)' value='{$linha['id']}' class='btn btn-sm btn-success'>{$lang['Responder']}</button>
														<button onclick='excluirMensagem(this)' value='{$linha['id']}' class='btn btn-sm btn-danger'>{$linha['Excluir']}</button>
													</p>
												</div>
											</div>
											</div>";
									}
								}
							?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
	<?php include 'footer.php'; ?>
</html>