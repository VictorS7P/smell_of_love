<?php 
	include 'head.php';
	include 'acessoRestrito.php';
?>
	<title>Smell of Love | <?php echo $lang["Sobre"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			include 'headerAdm.php';
			$sql = "SELECT * FROM sobre WHERE id = 1 LIMIT 1";
			$res = $con->query($sql);

			if ($res) {
				while ($linha = $res->fetch_assoc()) {					
		?>
		<section>
			<form method="POST" action="novoSobre.php">
				<div class="col-12 col-md-8 offset-md-2 sobre">
					<div class="row align-items-center mt-3">
						<div class="col-12">
							<div class="jumbotron py-3 rounded-0 item-sobre">
								<h2 class="display-4"><?php echo $lang["História"]; ?></h2>
								<h6 class="cor-branca">Pt-Br:</h6>
								<textarea id="historiaPt" name="historiaPt" class="w-100 fonte-normal"> <?php echo $linha["historiaPt"]; ?></textarea>
								<h6 class="cor-branca">En:</h6>
								<textarea id="historiaEn" name="historiaEn" class="w-100 fonte-normal"> <?php echo $linha["historiaEn"]; ?></textarea>
								<h6 class="cor-branca">Dn:</h6>
								<textarea id="historiaDn" name="historiaDn" class="w-100 fonte-normal"> <?php echo $linha["historiaDn"]; ?></textarea>								
							</div>
							</div>
						</div>
					<div class="row align-items-cente">
						<div class="col-12">
							<div class="jumbotron py-3 rounded-0 item-sobre">
								<h2 class="display-4"><?php echo $lang["Missão"]; ?></h2>
								<h6 class="cor-branca">Pt-Br:</h6>
								<textarea id="missaoPt" name="missaoPt" class="w-100 fonte-normal"> <?php echo $linha["missaoPt"]; ?></textarea>
								<h6 class="cor-branca">En:</h6>
								<textarea id="missaoEn" name="missaoEn" class="w-100 fonte-normal"> <?php echo $linha["missaoEn"]; ?></textarea>
								<h6 class="cor-branca">Dn:</h6>
								<textarea id="missaoDn" name="missaoDn" class="w-100 fonte-normal"> <?php echo $linha["missaoDn"]; ?></textarea>
							</div>
						</div>
					</div>

					<div class="row align-items-center">
						<div class="col-12">
							<div class="jumbotron py-3 rounded-0 item-sobre">
								<h2 class="display-4"><?php echo $lang["Visão"]; ?></h2>
								<h6 class="cor-branca">Pt-Br:</h6>
								<textarea id="visaoPt" name="visaoPt" class="w-100 fonte-normal"> <?php echo $linha["visaoPt"]; ?></textarea>
								<h6 class="cor-branca">En:</h6>
								<textarea id="visaoEn" name="visaoEn" class="w-100 fonte-normal"> <?php echo $linha["visaoEn"]; ?></textarea>
								<h6 class="cor-branca">Dn:</h6>
								<textarea id="visaoDn" name="visaoDn" class="w-100 fonte-normal"> <?php echo $linha["visaoDn"]; ?></textarea>
							</div>
						</div>
					</div>

					<div class="row align-items-center">
						<div class="col-12">
							<div class="jumbotron rounded-0 item-sobre">
								<h2 class="display-4"><?php echo $lang["Valores"]; ?></h2>
								<h6 class="cor-branca">Pt-Br:</h6>
								<textarea id="valoresPt" name="valoresPt" class="w-100 fonte-normal"> <?php echo $linha["valoresPt"]; ?></textarea>
								<h6 class="cor-branca">En:</h6>
								<textarea id="valoresEn" name="valoresEn" class="w-100 fonte-normal"> <?php echo $linha["valoresEn"]; ?></textarea>
								<h6 class="cor-branca">Dn:</h6>
								<textarea id="valoresDn" name="valoresDn" class="w-100 fonte-normal"> <?php echo $linha["valoresDn"]; ?></textarea>
							</div>
						</div>
					</div>
					<div class="row justify-content-center">
						<button type="submit" class="btn btn-padrao rounded-0 mb-3">Alterar Dados</button>
					</div>
				</div>				
			</form>
		</section>
		<?php 
				}
			}
			include 'footer.php';
		?>
</html>