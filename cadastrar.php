<?php 
	include 'head.php';
?>
	<title>Smell of Love | <?php echo $lang["Cadastrar"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php setarHeader(); ?>
		<section>
			<div class="col-12 col-md-8 offset-md-2">
			<div class="row">
				<div class="col-12">
			<div class="jumbotron item-sobre py-1 my-2 rounded-0" style="background: #276d5c">
				<form action="cadastro.php" method="POST" class="form-control rounded-0 border-0" style="background: rgba(255,255,255,0); color: white; text-shadow: 1px 1px 1px black;">
					<hr class="jumbotron-hr my-2">
					<h2 class="display-4 text-center py-2 fonte-normal"><?php echo $lang["Login e Senha"]; ?></h2>					
					<div class="form-group row justify-content-md-center">
						<label for="email" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["e-mail"]; ?>*
						</label>
						<div class="col-12 col-md-6 px-1">
							<div class="input-group">
								<input type="email" class="inputValidacao form-control rounded-0 fonte-normal border-right-0" name="email" id="email" required="required" onblur="validar(this)" >
								<div class='validacaoInput input-group-addon rounded-0 border-left-0 px-0'></div>
							</div>
							<div id="resposta-email" class="form-control-feedback fonte-normal"></div>
						</div>
					</div>

					<div class="form-group row justify-content-md-center">
						<label for="senha" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["Senha"]; ?>*
						</label>
						<div class="pull-md-1 col-12 col-md-4 px-1">
						<div class="input-group">
							<input type="password" class="inputValidacao border-right-0 form-control rounded-0 fonte-normal" name="senha" id="senha" required="required" onkeyup="verificarSenha(this)">
							<span class="border-left-0 fa fa-eye input-group-addon rounded-0" onmouseenter="mostrarSenha('senha')" onmouseleave="esconderSenha()" onclick="mostrarSenha('senha')" style="text-shadow: none"></span>
							<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
						</div>
						<div class="progress mt-1 w-100">
							<div id="barraSenha" class="progress-bar bg-danger" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="height: 10px;"></div>							
						</div>
						</div>
					</div>

					<div class="form-group row justify-content-md-center">
						<label for="confirmaSenha" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["Confirme sua senha"]; ?>*
						</label>
						<div class="pull-md-1 col-12 col-md-4 px-1">
							<div class="input-group">
								<input type="password" class="inputValidacao border-right-0 form-control rounded-0 fonte-normal" name="confirmaSenha" id="confirmaSenha" required="required" onkeyup="validar(this)">
								<span class="border-left-0 fa fa-eye input-group-addon rounded-0" onmouseenter="mostrarSenha('confirmaSenha')" onmouseleave="esconderSenha()" onclick="mostrarSenha('confirmaSenha')" style="text-shadow: none"></span>
								<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
							</div>
							<div class="form-control-feedback fonte-normal"></div>
						</div>
					</div>
					<hr class="jumbotron-hr my-2">
					<h2 class="display-4 fonte-normal text-center py-2"><?php echo $lang["Informações Pessoais"]; ?></h2>
					<div class="form-group row justify-content-md-center">
						<label for="nome" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["Nome completo"]; ?>*
						</label>
						<div class="col-12 col-md-6 px-1">
						<div class="input-group">
							<input type="text" class="inputValidacao border-right-0 form-control rounded-0 fonte-normal" name="nome" id="nome" required="required" onblur="validar(this)" onblur="validar(this)">
							<div class='validacaoInput input-group-addon rounded-0 border-left-0 px-0'></div>
						</div>
						</div>
						<div class="form-control-feedback fonte-normal"></div>
					</div>

					<div class="form-group row justify-content-md-center">
						<div class="col-6 col-md-4 fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["Sexo"]; ?>*
						</div>
						<div class="col-12 col-md-6 px-1">
							<div class="form-check form-check-inline">
								<label for="sexoM" class="form-check-label fonte-normal custom-control custom-radio">
									<input type="radio" class="form-check-input custom-control-input" name="sexo" id="sexoM" value="M" required="required" onclick="validar(this)">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description fonte-normal"><?php echo $lang["Masculino"]; ?></span">
								</label>
							</div>
							<div class="form-check form-check-inline">
								<label for="sexoF" class="form-check-label fonte-normal custom-control custom-radio">
									<input type="radio" class="form-check-input custom-control-input bg-danger" name="sexo" id="sexoF" value="F" onclick="validar(this)">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description fonte-normal"><?php echo $lang["Feminino"]; ?></span">
								</label>
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-md-center">
						<label for="cpf" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["CPF"]; ?>*
						</label>						
						<div class="pull-md-1 col-12 col-md-4 px-1">
							<div class="input-group">
								<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="cpf" id="cpf" placeholder="___.___.___.__" required="required" onblur="validar(this)">
								<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
							</div>
							<div class="resposta-cpf form-control-feedback fonte-normal"></div>
						</div>
					</div>

					<div class="form-group row justify-content-md-center">
						<label for="dataNascimento" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["Data de nascimento"]; ?>*
						</label>
						<div class="pull-md-1 col-12 col-md-4 px-1">
							<div class="input-group">
								<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="dataNascimento" id="dataNascimento" required="required" placeholder="__/__/_____" onblur="validar(this)" onblur="validar(this)">
								<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-md-center">
						<label for="telContato" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["Telefone para contato"]; ?>*
						</label>
						<div class="pull-md-1 col-12 col-md-4 px-1">
							<div class="input-group">
								<input type="tel" class="inputValidacao border-right-0 form-control rounded-0 fonte-normal" name="telContato" id="telContato" required="required" placeholder="(__) _____-____" onblur="validar(this)" onblur="validar(this)">
								<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-md-center">
						<label for="telCelular" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
							<?php echo $lang["Telefone celular"]; ?>
						</label>
						<div class="pull-md-1 col-12 col-md-4 px-1">
							<div class="input-group">
								<input type="tel" class="inputValidacao form-control rounded-0 fonte-normal" name="telCelular" id="telCelular" placeholder="(__) _____-____" onblur="validar(this)" onblur="validar(this)">
								<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-md-center pb-0 mb-0">
						<div class="offset-md-5 col-12 col-md-6 px-1">
							<div class="form-check form-check-inline">
								<label for="receberOfertas" class="form-check-label fonte-normal custom-control custom-checkbox">
									<input type="checkbox" class="form-check-input custom-control-input" name="receberOfertas" id="receberOfertas" checked="checked">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description fonte-normal"><?php echo $lang["Desejo receber ofertas da Smell por e-mail"]; ?>.</span">
								</label>
							</div>
						</div>
					</div>

					<div class="form-group row justify-content-md-center">
						<div class="offset-md-5 col-12 col-md-6 px-1">
							<div class="form-check form-check-inline">
								<label for="receberOfertasCelular" class="form-check-label fonte-normal custom-control custom-checkbox">
									<input type="checkbox" class="form-check-input custom-control-input" name="receberOfertasCelular" id="receberOfertasCelular" checked="checked">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description fonte-normal"><?php echo $lang["Desejo receber ofertas da Smell pelo celular"]; ?>.</span>
								</label>
							</div>
						</div>
					</div>
					<hr class="jumbotron-hr">
					<h2 class="display-4 fonte-normal py-2 text-center"><?php echo $lang["Informações para Entrega"]; ?></h2>
					<div class="form-group row justify-content-md-center">
						<label for="cep" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1">
							<?php echo $lang["CEP"]; ?>*
						</label>
						<div class="pull-md-1 col-12 col-md-3 px-1">
							<div class="input-group">
								<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="cep" id="cep" required="required" placeholder="_____-___" onkeyup="validar(this)" onblur="pesquisacep(this.value)">
								<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
							</div>
						</div>
					</div>

					<div id="endereco">
						<div class="form-group row justify-content-md-center">
							<label for="" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
								<?php echo $lang["Rua"]; ?>*
							</label>
							<div class="col-10 col-md-4 px-1">
								<div class="input-group">
									<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="rua" id="rua" required="required" readonly="readonly">																
									<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
								</div>
							</div>
							<div class="col-2 col-md-2 px-1">
								<div class="input-group">
									<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="nCasa" id="nCasa" required="required" placeholder="Nº" onblur="validar(this)">																
									<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
								</div>
							</div>
						</div>

						<div class="form-group row justify-content-md-center">
							<label for="complemento" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
								<?php echo $lang["Complemento"]; ?>
							</label>
							<div class="col-12 col-md-6 px-1">
								<div class="input-group">
									<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="complemento" id="complemento" onblur="validar(this)">
									<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
								</div>
							</div>
						</div>

						<div class="form-group row justify-content-md-center">
							<label for="" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
								<?php echo $lang["Bairro"]; ?>*
							</label>
							<div class="col-12 col-md-6 px-1">
								<div class="input-group">
									<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="bairro" id="bairro" required="required" readonly="readonly">																
									<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
								</div>
							</div>
						</div>

						<div class="form-group row justify-content-md-center">
							<label for="" class="offset-md-3 col-6 col-md-2 col-form-label fonte-normal text-md-right px-1">
								<?php echo $lang["Cidade"]; ?>*
							</label>
							<div class="col-10 col-md-4 px-1">
								<div class="input-group">
									<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="cidade" id="cidade" required="required" readonly="readonly">																
									<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
								</div>
							</div>
							<div class="col-2 px-1">
								<div class="input-group">
									<input type="text" class="inputValidacao form-control rounded-0 fonte-normal" name="uf" id="uf" required="required" readonly="readonly">																
									<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
								</div>
							</div>
						</div>					
					</div>					
					<div class="row justify-content-center">
						<button id="cadastrar" type="submit" class="btn btn-success rounded-0" disabled="disabled"><?php echo $lang["Cadastrar"]; ?></button>
					</div>
					<hr class="jumbotron-hr">
				</form>
			</div>
			</div>
			</div>
			</div>
		</section>
		<?php include 'footer.php'; ?>
</html>