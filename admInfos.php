<?php 
	include 'head.php';
	include 'acessoRestrito.php';
?>
	<title>Smell of Love |<?php echo $lang["Informações Gerais"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php include 'headerAdm.php'; ?>
		<section>
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row mx-2">
					<div class="col-12">
						<h2 class="text-center my-3"><?php echo $lang["Alterar informações gerais do site"]; ?></h2>
						<table class="table table-bordered table-hover table-striped my-2">
							<thead class="thead-inverse">
								<tr>
									<th><?php echo $lang["Item"]; ?></th>
									<th><?php echo $lang["Valor atual"]; ?></th>
									<th><?php echo $lang["Alterar"]; ?></th>
								</tr>
							</thead>
							<tbody>								
								<?php 
									$sql = "SELECT * FROM smell";
									$res = $con->query($sql);

									if ($res) {
										while ($linha = $res->fetch_assoc()) { 
											echo "
												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["e-mail"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["email"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["2º e-mail"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["email2"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Telefone"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["telefone"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["2º telefone"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["telefone2"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Celular"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["celular"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Link página Facebook"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["linkFacebook"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Link Instagram"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["linkInstagram"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Snapchat"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["linkSnapchat"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Rua"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["rua"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>
												
												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Nº"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["numero"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Bairro"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["bairro"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["CEP"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["cep"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>

												<tr>
													<td class='tipoInformacao w-25 fonte-normal'>{$lang["Cidade"]}</td>
													<td class='dadoInformacao w-75 fonte-normal'>{$linha["cidade"]}</td>
													<td class='w-25 text-center'>
														<button onclick='trocarInformacao(this)' class='btn btn-sm btn-success' data-toggle='modal' data-target='#alterarDado'>
															<span class='fa fa-refresh'></span>
														</button>
													</td>
												</tr>
											";
										}
									}
									mysqli_close($con);
								?>						
							</tbody>
						</table>
					</div>
				</div>
			</div>

		<div id="alterarDado" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header justify-content-center">
						<div class="modal-title titulo-modal">
							<h2>
								Alterar Dado
							</h2>
						</div>
					</div>

					<div class="modal-body">
						<form action="alterarDado.php" method="POST">
						<div class="row justify-content-center mx-3">
							<input id="tipoInformacaoModal" name="tipoInformacaoModal" class="fonte-normal w-25 form-control" readonly="readonly"></input>
							<input id="dadoInformacaoModal" name="dadoInformacaoModal" type="text" class="fonte-normal w-50 form-control mx-2" value="smelloflove@gmail.com" onblur="validar(this)">
							<button type="submit" class='btn btn-sm btn-success rounded-0' data-toggle='modal' data-target='#alterarDado'>
								<span>Alterar</span>
							</button>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		</section>
		<?php include 'footer.php'; ?>
</html>