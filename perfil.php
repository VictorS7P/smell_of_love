<?php 
	include 'head.php';
?>
	<title>Smell of Love | <?php echo $lang["Meus Dados"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			setarHeader();

			if (empty($_SESSION['id'])) {
				echo "
					<script>
						alert({$lang["Faça login para continuar"]});
						window.location.href = 'index.php';
					</script>
				";
			}

			$sql = "SELECT * FROM usuario WHERE id = '{$_SESSION['id']}'";
			$res = $con->query($sql);

			if ($res) {
				while ($linha = $res->fetch_assoc()) {

		?>
			<section>
				<div class="col-12 col-md-8 offset-md-2">
					<div class="row">
						<div class="col-12">
							<div class="jumbotron item-sobre py-1 my-2 rounded-0" style="background: #276d5c">
								<div action="" class="form-control rounded-0 border-0" style="background: rgba(255,255,255,0); color: white; text-shadow: 1px 1px 1px black;">
									<h2 class="display-3 text-center py-2 fonte-normal"><?php echo $lang["Meus Dados"]; ?></h2>
									<hr class="jumbotron-hr my-2">
									<h2 class="display-4 text-center py-2 fonte-normal"><?php echo $lang["Login e Senha"]; ?></h2>					
									<div class="form-group row justify-content-md-center">
										<label for="email" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
											<?php echo $lang["e-mail"]; ?>
										</label>
										<div class="col-12 col-md-6 px-1">
											<input type="email" class="form-control rounded-0 fonte-normal" name="email" id="email" readonly="readonly" value="<?php echo $linha['email'];?>">
										</div>
									</div>
									
									<div id="conjunto-1">
										<div class="form-group row justify-content-md-center">
											<label for="antigaSenha" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
												<span id="titulo-1" class="fonte-normal"><?php echo $lang["Senha"]; ?></span>
											</label>
											<div class="pull-md-1 col-12 col-md-3 px-1">
												<div class="input-group">
													<input type="password" class="form-control rounded-0 fonte-normal" name="antigaSenha" id="antigaSenha" readonly="readonly" placeholder="********">
												</div>
											</div>
											<div class="pull-md-1 col-1 px-1 pt-2 pt-md-0">
												<button class="btn btn-success rounded-0" value="1" data-toggle="modal" data-target="#alterarSenha"><?php echo $lang["Alterar senha"]; ?></button>
											</div>
										</div>
									</div>
									
									<hr class="jumbotron-hr my-2">
									<h2 class="display-4 fonte-normal text-center py-2" id="titulo-2"><?php echo $lang["Informações Pessoais"]; ?></h2>
									<div id="conjunto-2">
										<div class="form-group row justify-content-md-center">
											<label for="nome" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
												<?php echo $lang["Nome"]; ?>
											</label>
											<div class="col-12 col-md-6 px-1">
												<input type="text" class="form-control rounded-0 fonte-normal" name="nome" id="nome" value="<?php echo $linha['nome'];?>">
											</div>
										</div>

										<div class="form-group row justify-content-md-center">
											<div class="col-6 col-md-4 fonte-normal text-md-right px-1 offset-md-1">
												<?php echo $lang["Sexo"]; ?>
											</div>
											<div class="col-12 col-md-6 px-1">
												<div class="form-check form-check-inline">
													<label for="sexoM" class="form-check-label fonte-normal custom-control custom-radio">
														<input type="radio" class="form-check-input custom-control-input" name="sexo" id="sexoM" value="m" <?php checarRadios($linha['sexo'], 'M') ?>>
														<span class="custom-control-indicator"></span>
														<span class="custom-control-description fonte-normal"><?php echo $lang["Masculino"]; ?></span>
													</label>
												</div>
												<div class="form-check form-check-inline">
													<label for="sexoF" class="form-check-label fonte-normal custom-control custom-radio">
														<input type="radio" class="form-check-input custom-control-input bg-danger" name="sexo" id="sexoF" value="f" <?php checarRadios($linha['sexo'], 'F') ?>>
														<span class="custom-control-indicator"></span>
														<span class="custom-control-description fonte-normal"><?php echo $lang["Feminino"]; ?></span>
													</label>
												</div>
											</div>
										</div>

										<div class="form-group row justify-content-md-center">
											<label for="cpf" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
												<?php echo $lang["CPF"]; ?>
											</label>
											<div class="pull-md-1 col-12 col-md-4 px-1">
												<input type="text" class="form-control rounded-0 fonte-normal" name="cpf" id="cpf" value="<?php echo $linha['cpf'];?>">
											</div>
										</div>

										<div class="form-group row justify-content-md-center">
											<label for="dataNascimento" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
												<?php echo $lang["Data de nascimento"]; ?>
											</label>
											<div class="pull-md-1 col-12 col-md-4 px-1">
												<input type="text" class="form-control rounded-0 fonte-normal" name="dataNascimento" id="dataNascimento" value="<?php echo $linha['dataNascimento'];?>">
											</div>
										</div>

										<div class="form-group row justify-content-md-center">
											<label for="telContato" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
												<?php echo $lang["Telefone para contato"]; ?>
											</label>
											<div class="pull-md-1 col-12 col-md-4 px-1">
												<input type="tel" class="form-control rounded-0 fonte-normal" name="telContato" id="telContato" value="<?php echo $linha['telefoneContato'];?>">
											</div>
										</div>

										<div class="form-group row justify-content-md-center">
											<label for="telCelular" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
												<?php echo $lang["Telefone celular"]; ?>
											</label>
											<div class="pull-md-1 col-12 col-md-4 px-1">
												<input type="tel" class="form-control rounded-0 fonte-normal" name="telCelular" id="telCelular" <?php if ($linha['telefoneCelular'] == '') {echo 'placeholder="Não cadastrado"';} else {echo 'value="' . $linha['telefoneCelular'] . '"';} ?>">
											</div>
										</div>

										<div class="row justify-content-md-center">
											<div class="col-2 px-1">
												<button class="btn btn-success rounded-0" value="2" onclick="valorModalPerfil(2)"><?php echo $lang["Alterar Dados Pessoais"]; ?></button>
											</div>
										</div>
									</div>

									<hr class="jumbotron-hr">
									<h2 class="display-4 fonte-normal py-2 text-center" id="titulo-3"><?php echo $lang["Informações para Entrega"]; ?></h2>
									<div id="conjunto-3">
										<div class="form-group row justify-content-md-center">
											<label for="cep" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1">
												<?php echo $lang["CEP"]; ?>
											</label>
											<div class="pull-md-1 col-12 col-md-3 px-1">
												<input type="text" class="form-control rounded-0 fonte-normal" name="cep" id="cep" placeholder="_____-___" onblur="pesquisacep(this.value)" value="<?php echo $linha['cep']; ?>" >
											</div>
										</div>
										<div id="conjunto-3">
											<div class="form-group row justify-content-md-center">
												<label for="" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
													<?php echo $lang["Rua"]; ?>
												</label>
												<div class="col-10 col-md-5 px-1">
													<input type="text" class="form-control rounded-0 fonte-normal" name="rua" id="rua" readonly="readonly" readonly="readonly" value="<?php echo $linha['rua']; ?>">
												</div>
												<div class="col-2 col-md-1 px-1">
													<input type="text" class="form-control rounded-0 fonte-normal" name="nCasa" id="nCasa" placeholder="<?php echo $lang["Nº"]; ?>" value="<?php echo $linha['numero']; ?>">
												</div>
											</div>

											<div class="form-group row justify-content-md-center">
												<label for="complemento" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
													<?php echo $lang["Complemento"]; ?>
												</label>
												<div class="col-12 col-md-6 px-1">
													<input type="text" class="form-control rounded-0 fonte-normal" name="complemento" id="complemento" value="<?php echo $linha['complemento']; ?>">
												</div>
											</div>

											<div class="form-group row justify-content-md-center">
												<label for="" class="col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
													<?php echo $lang["Bairro"]; ?>
												</label>
												<div class="col-12 col-md-6 px-1">
													<input type="text" class="form-control rounded-0 fonte-normal" name="bairro" id="bairro" readonly="readonly" value="<?php echo $linha['bairro']; ?>">
												</div>
											</div>

											<div class="form-group row justify-content-md-center">
												<label for="" class="offset-md-3 col-6 col-md-2 col-form-label fonte-normal text-md-right px-1">
													<?php echo $lang["Cidade"]; ?>
												</label>
												<div class="col-10 col-md-4 px-1">
													<input type="text" class="form-control rounded-0 fonte-normal" name="cidade" id="cidade" readonly="readonly" value="<?php echo $linha['cidade']; ?>">
												</div>
												<div class="col-2 px-1">
													<input type="text" class="form-control rounded-0 fonte-normal" name="uf" id="uf" readonly="readonly" value="<?php echo $linha['uf']; ?>">
												</div>
											</div>					

											<div class="row justify-content-md-center">
												<div class="col-2 px-1">
													<button class="btn btn-success rounded-0" value="3" onclick="valorModalPerfil(3)"><?php echo $lang["Alterar Dados de Entrega"]; ?></button>
												</div>
											</div>
										</div>
										<hr class="jumbotron-hr">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			<div id="alterarSenha" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header justify-content-center">
							<div class="modal-title titulo-modal text-center">
								<h2>
									<?php echo $lang["Alterar Senha"]; ?>
								</h2>								
							</div>
						</div>
						<div class="modal-body" id="body-modal">
							<div class="jumbotron item-sobre py-1 my-2 rounded-0" style="background: #276d5c" id="modal-nova-senha">
								<div class="form-group row justify-content-md-center mt-2">
									<label for="senha" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
										<?php echo $lang["Digite uma nova senha"]; ?>*
									</label>
									<div class="pull-md-1 col-12 col-md-4 px-1">
									<div class="input-group">
										<input type="password" class="inputValidacao border-right-0 form-control rounded-0 fonte-normal" name="senha" id="senha" required="required" onkeyup="verificarSenha(this)">
										<span class="border-left-0 fa fa-eye input-group-addon rounded-0" onmouseenter="mostrarSenha('senha')" onmouseleave="esconderSenha()" onclick="mostrarSenha('senha')" style="text-shadow: none"></span>
										<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
									</div>
									<div class="progress mt-1 w-100">
										<div id="barraSenha" class="progress-bar bg-danger" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="height: 10px;"></div>							
									</div>
									</div>
								</div>

								<div class="form-group row justify-content-md-center">
									<label for="confirmaSenha" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
										<?php echo $lang["Confirme sua nova senha"]; ?>*
									</label>
									<div class="pull-md-1 col-12 col-md-4 px-1">
										<div class="input-group">
											<input type="password" class="inputValidacao border-right-0 form-control rounded-0 fonte-normal" name="confirmaSenha" id="confirmaSenha" required="required">
											<span class="border-left-0 fa fa-eye input-group-addon rounded-0" onmouseenter="mostrarSenha('confirmaSenha')" onmouseleave="esconderSenha()" onclick="mostrarSenha('confirmaSenha')" style="text-shadow: none"></span>
											<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
										</div>
										<div class="form-control-feedback fonte-normal"></div>
									</div>
								</div>

								<div class="form-group row justify-content-md-center">
									<label for="confirmaSenha" class="pull-md-1 col-6 col-md-4 col-form-label fonte-normal text-md-right px-1 offset-md-1">
										<?php echo $lang["Digite sua antiga senha"]; ?>
									</label>
									<div class="pull-md-1 col-12 col-md-4 px-1">
										<div class="input-group">
											<input type="password" class="inputValidacao border-right-0 form-control rounded-0 fonte-normal" name="antigaSenhaModal" id="antigaSenhaModal" required="required">
											<span class="border-left-0 fa fa-eye input-group-addon rounded-0" onmouseenter="mostrarSenha('antigaSenhaModal')" onmouseleave="esconderSenha()" onclick="mostrarSenha('antigaSenhaModal')" style="text-shadow: none"></span>
											<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
										</div>
										<div class="form-control-feedback fonte-normal"></div>
									</div>
								</div>

								<div class="form-group row justify-content-md-center">
									<button id="alterar" class="btn btn-padrao rounded-0"  onclick="alterarDadoUser(1)">
										<?php echo $lang["alterar"]; ?>
									</button>
								</div>
							</div>			
						</div>						
					</div>
				</div>
			</div>

			<div id="modal-senha-antiga" class="modal fade" role="dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header justify-content-center">
							<div class="modal-title titulo-modal text-center">
								<h2>
									<?php echo $lang["Alterar Dados"]; ?>
								</h2>								
							</div>
						</div>
						<div class="modal-body" id="body-modal">
							<div class="jumbotron item-sobre py-1 my-2 rounded-0" style="background: #276d5c" id="modal-nova-senha">
								<div class="form-group row justify-content-md-center my-0">
									<label for="senhaModal" class="col-12 col-form-label fonte-normal text-center px-1">
										<?php echo $lang["Digite sua senha para continuar"]; ?>
									</label>
									<div class="col-4">
										<div class="input-group">
											<input type="password" class="inputValidacao border-right-0 form-control rounded-0 fonte-normal" name="senhaModal" id="senhaModal" required="required">
											<span class="border-left-0 fa fa-eye input-group-addon rounded-0" onmouseenter="mostrarSenha('senhaModal')" onmouseleave="esconderSenha()" onclick="mostrarSenha('senhaModal')" style="text-shadow: none"></span>
											<span class="validacaoInput border-left-0 input-group-addon rounded-0 px-0"></span>
										</div>
										<div class="form-control-feedback fonte-normal"></div>
									</div>
								</div>

								<div class="form-group row justify-content-md-center my-1">
									<button id="alterarDadosBtn" class="btn btn-padrao rounded-0" onclick="">
										<?php echo $lang["alterar"]; ?>
									</button>
								</div>
							</form>			
						</div>						
					</div>
				</div>
			</div>

			</section>
		<?php
				}
			}
			include 'footer.php';
		?>
</html>