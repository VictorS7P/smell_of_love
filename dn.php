<?php 
	// admCupons.php
	$lang["cupons de desconto"] = "Kupons af nedslag";
	$lang["cadastrar novo cupom"] = "Registrere ny kupon";
	$lang["Código"] = "Kode";
	$lang["Valor Fixo"] = "Valør";
	$lang["Valor Percentual"] = "Valør Procent";
	$lang["Cadastrar"] = "Registrere";
	$lang["O código deve conter 8 números"] = "Kode must have 8 nummers";
	$lang["Cupons cadastrados"] = "Kupons registreret";
	$lang["Descrição"] = "Beskrivelse";
	$lang["Deletar"] = "Slette";
	$lang["descricao"] = "descricaoDn"; // Não mecher

	// admInfos.php
	$lang["Informações Gerais"] = "Melding General";
	$lang["Alterar informações gerais do site"] = "Skift melding general sted";
	$lang["Item"] = "Item";
	$lang["Valor atual"] = "Valør virkelig";
	$lang["Alterar"] = "Skift";
	$lang["e-mail"] = "Mail";
	$lang["2º e-mail"] = "2º Mail";
	$lang["Telefone"] = "Telefon";
	$lang["2º telefone"] = "2º telefon";
	$lang["Celular"] = "Uro telefon";
	$lang["Link página Facebook"] = "Forbindelse side Facebook";
	$lang["Link Instagram"] = "Forbindelse Instagram";
	$lang["Snapchat"] = "Snapchat";
	$lang["Rua"] = "Gade";
	$lang["Nº"] = "Nummer";
	$lang["Bairro"] = "Nabolag";
	$lang["CEP"] = "Postnumre";
	$lang["Cidade"] = "By";
	$lang["Alterar Dado"] = "Skift data";
	$lang["Alterar"] = "Skift";

	// administrador.php
	$lang["Painel"] = "Panel";
	$lang["Painel Administrativo"] = "Panel Bestyrer";
	$lang["Acessar Site"] = "Adgang Sted";
	$lang["Alterar/Cadastrar Produtos"] = "Skift/Registrere Produkts";
	$lang["Alterar Sobre"] = "Skift Om";
	$lang["Alterar Cupons de Desconto"] = "Skyft Kupons af nedslag";
	$lang["Alterar Informações Gerais"] = "Skift Melding General";
	$lang["novas mensagens"] = "Nys beskeders";
	$lang["Adicionar Promoção"] = "Addere Forfremmelse";

	// admMensagens.php
	$lang["Mensagens"] = "Beskeders";
	$lang["Arquivadas"] = "Arkiveret";
	$lang["Arquivar"] = "Arkivere";
	$lang["Responder"] = "Svar";
	$lang["Excluir"] = "Slette";
	$lang["Mensagens Arquivadas"] = "Arkiveret Beskeders";
	$lang["Recarregue a página para atualizar as mensagens"] = "Friske op et side for opdatering som beskeders";

	// admProdutos.php
	$lang["Imagem nas dimensões erradas... Cadastre somente imagens 400x200..."] = "Image inde dimension uret... Registrere eneste image 400x200...";
	$lang["Produtos"] = "Produkts";
	$lang["Cadastrar Produto"] = "Registrere Produkts";
	$lang["Masculinos"] = "Herrer";
	$lang["Produtos Cadastrados"] = "Produkts Registreret";
	$lang["Femininos"] = "Damen";
	$lang["Ordenar Por"] = "Befaling Af";
	$lang["Nada"] = "Intet";
	$lang["Estoque"] = "Lager";
	$lang["Quantidade de vendas"] = "Kvantitet af sælge";
	$lang["Avaliação"] = "Evaluering";
	$lang["Preço"] = "Pris";
	$lang["Ver Todos"] = "Se Alt";
	$lang["Masculino"] = "Mand";
	$lang["Feminino"] = "Kvinde";
	$lang["em estoque"] = "inde lager";
	$lang["vendidos"] = "solgt";
	$lang["Alterar/Excluir"] = "Skift/Slette";
	$lang["Cadastrar novo produto"] = "Registrere ny produkt";
	$lang["Nome"] = "Navn";
	$lang["Nome do produto..."] = "Navn produkt...";
	$lang["Quantidade"] = "Kvantitet";
	$lang["Imagem"] = "Imagem";
	$lang["Tamanho"] = "Størrelse";
	$lang["Tipo"] = "Type";
	$lang["Descrição"] = "Beskrivelse";
	$lang["Descrição em português"] = "Beskrivelse inde portugiser";
	$lang["Descrição em inglês"] = "Beskrivelse inde engelsk";
	$lang["Descrição em dinamarquês"] = "Beskrivelse inde wienerbrød";
	$lang["Conteúdo do frasco (em ml)"] = "Volumen inde flaske (inde ml)";
	$lang["Preço (em R$)"] = "Pris (inde R$)";
	$lang["Cadastrar"] = "Registrere";
	$lang["Alterar/Excluir produto"] = "Skift/Slette produkt";
	$lang["Conteúdo do frasco"] = "Volumen af den flaske";
	$lang["Estoque"] = "Lager";
	$lang["Preço"] = "Pris";
	$lang["Alterar"] = "Skift";
	$lang["Excluir"] = "Slette";
	$lang["Nome"] = "Navn";
	$lang["Somente números"] = "Eneste nummers";

	// admPromocao.php
	$lang["Promoções"] = "Forfremmelse";
	$lang["Produtos em Promoção"] = "Produkts inde Forfremmelse";
	$lang["Sempre que um produto tiver um preço menor definido ele será considerado em promoção"] = "Altid et produkt has den lavere pris definere vilje være overveje inde forfremmelse";
	$lang["Conteúdo"] = "Volumen";
	$lang["Alterar Preço"] = "Skift Pris";
	$lang["Outros Produtos"] = "øvrig produkts";
	$lang["Novo Preço"] = "Ny Pris";
	$lang["Ok"] = "Ok";

	// admSobre.php
	$lang["Sobre"] = "Om";
	$lang["História"] = "Historie";
	$lang["novaHistoriaPt"] = "Type ny historie (inde portugiser), Afsked formular for ikke skift valør!";
	$lang["novaHistoriaEn"] = "Type ny historie (inde engelsk), Afsked formular for ikke skift valør!";
	$lang["novaHistoriaDn"] = "Type ny historie (inde wienerbrød), Afsked formular for ikke skift valør!";
	$lang["Missão"] = "Mission";
	$lang["novaMissãoPt"] = "Type ny Mission (inde portugiser), Afsked formular for ikke skift valør!";
	$lang["novaMissãoEn"] = "Type ny Mission (inde engelsk), Afsked formular for ikke skift valør!";
	$lang["novaMissãoDn"] = "Type ny Mission (inde wienerbrød), Afsked formular for ikke skift valør!";
	$lang["Visão"] = "Vision";
	$lang["novaVisãoPt"] = "Type ny Vision (inde portugiser), Afsked formular for ikke skift valør!";
	$lang["novaVisãoEn"] = "Type ny Vision (inde engelsk), Afsked formular for ikke skift valør!";
	$lang["novaVisãoDn"] = "Type ny Vision (inde wienerbrød), Afsked formular for ikke skift valør!";
	$lang["Valores"] = "Værdier";
	$lang["novosValoresPt"] = "Type ny Valør (inde portugiser), Afsked formular for ikke skift valør!";
	$lang["novosValoresEn"] = "Type ny Valør (inde engelsk), Afsked formular for ikke skift valør!";
	$lang["novosValoresDn"] = "Type ny Valør (inde wienerbrød), Afsked formular for ikke skift valør!";
	$lang["AlterarDados"] = "Skift Data";

	// cadastrar.php
	$lang["Cadastrar"] = "Registrere";
	$lang["Login e Senha"] = "Mail og Kodeord";
	$lang["Senha"] = "Kodeord";
	$lang["Confirme sua senha"] = "Bekræfte din kodeord";
	$lang["Informações Pessoais"] = "Melding Personlig";
	$lang["Nome completo"] = "Fuld Navn";
	$lang["Sexo"] = "Sex";
	$lang["CPF"] = "CPF";
	$lang["Data de nascimento"] = "Fødsel date";
	$lang["Telefone para contato"] = "Kontakt Telefon";
	$lang["Telefone celular"] = "Uro Telefon";
	$lang["Desejo receber ofertas da Smell por e-mail"] = "Modtage tilbud af Smell inde din mail.";
	$lang["Desejo receber ofertas da Smell pelo celular"] = "Modtage tilbud af Smell inde din ure telefon.";
	$lang["Informações para Entrega"] = "Levering Melding";
	$lang["CEP"] = "Postnumre";
	$lang["Rua"] = "Gade";
	$lang["Complemento"] = "Komplement";
	$lang["Bairro"] = "Nabolag";
	$lang["Cidade"] = "By";

	// checkout.php
	$lang["Checkout"] = "Kasse";
	$lang["Você não tem produtos no carrinho"] = "Ingen produkt inde vogn";
	$lang["Minha sacola de compras"] = "Min shopping pose";
	$lang["Produto"] = "Produkt";
	$lang["Valor"] = "Valør";
	$lang["Quantidade"] = "Kvantitet";
	$lang["Total"] = "Total";
	$lang["Excluir"] = "Slette";
	$lang["Subtotal"] = "Subtotal";
	$lang["Desconto"] = "Nabolag";
	$lang["Frete"] = "Fragt";
	$lang["Total"] = "Total";
	$lang["OK"] = "OK";
	$lang["Calcular"] = "Beregne";
	$lang["Finalizar Compra"] = "Slutning";
	$lang["Digite seu cupom de desconto"] = "Type din kupon af nedslag";
	$lang["Digite seu CEP"] = "Type din postnumre";

	// contato.php
	$lang["Contato"] = "Kontakt";
	$lang["Fale Conosco"] = "Kontakt Vi";
	$lang["Mensagem"] = "Budskab";
	$lang["Enviar"] = "Sende";

	// feminino.php
	$lang["Produtos Femininos"] = "Damen Produkts";
	$lang["Adicionar ao carrinho"] = "Addere for vogn";
	$lang["Nenhum Produto Feminino Cadastrado"] = "Ingen Damen Produkts Registreret";

	// footer.php
	$lang["Nossas redes sociais"] = "Vor Social Nets";
	$lang["Fale conosco"] = "Kontakt Vi";
	$lang["Formas de pagamento"] = "Betale Af";
	$lang["Todos os direitos reservados"] = "Alt rettigheder afvisende";
	$lang["nº"] = "nummer";

	// head.php
	$lang["lang"] = "pt-bt";

	// header.php
	$lang["Login"] = "Login";
	$lang["Criar uma conta"] = "Skabe konta";
	$lang["Meu carrinho"] = "Min Vogn";
	$lang["Finalizar"] = "Slutning";
	$lang["Pesquisar"] = "Ransage";
	$lang["Continuar online"] = "Ophold online";
	$lang["Logar"] = "Login";
	$lang["E-mail"] = "Mail";
	$lang["Senha"] = "Kodeord";

	// headerAdm.php
	$lang["Área do administrador"] = "Bestyrer Areal";
	$lang["Sair"] = "Tegn Ude";

	// headerLogado.php
	$lang["Perfil"] = "Profil";
	$lang["Administrativo"] = "Bestyrer";
	$lang["Meu carrinho"] = "Min Vogn";
	$lang["Bem-vindo"] = "Velkomst";

	// index.php
	$lang["link-slide1"] = "_assets/imagens/imagem-slide-1.png";
	$lang["Home"] = "Hjem";
	$lang["Promoções"] = "Forfremmelse";
	$lang["Últimos Produtos Femininos"] = "Læst Damer Produkts";
	$lang["Ver todos"] = "Se Alt";
	$lang["Últimos Produtos Masculinos"] = "Læst Herrer Produkts";

	// masculino.php
	$lang["Produtos Masculinos"] = "Herrer Produkts";
	$lang["Nenhum Produto Masculino Cadastrado"] = "Ingen mand produkt registreret";

	// perfil.php
	$lang["Meu Perfil"] = "Min Profil";
	$lang["Faça login para continuar"] = "Lave login for vare";
	$lang["Meus Dados"] = "Min Data";
	$lang["Alterar senha"] = "Skift Kodeord";
	$lang["Alterar Dados Pessoais"] = "Skift Personal Data";
	$lang["Alterar Dados de Entrega"] = "Skift Levering data";
	$lang["Alterar Senha"] = "Skift kodeord";
	$lang["Digite uma nova senha"] = "Type ny kodeord";
	$lang["Confirme sua nova senha"] = "Bekræfte din ny kodeord";
	$lang["Digite sua antiga senha"] = "Type din gammel kodeord";
	$lang["alterar"] = "skift";
	$lang["Alterar Dados"] = "Skift data";
	$lang["Digite sua senha para continuar"] = "Type din kodeord for vare";
	$lang["alterar"] = "skift";

	// pesquisa.php
	$lang["Pesquisa"] = "Ransage";
	$lang["Resultados da busca para"] = "Ransage resultater til";
	$lang["Nenhum Resultado Encontrado"] = "Ingen resultater oprette";

	// produto.php
	$lang["em estoque"] = "inde lager";
	$lang["vendidos"] = "solgt";
	$lang["Sua avaliação atual"] = "Din vurdere virkelig";
 	$lang["Não avaliado ainda"] = "Nej vurdere endnu";
 	$lang["Comentários"] = "Kommentar";
 	$lang["Nenhum comentário até o momento"] = "Ingen kommentar indtil nu";
 	$lang["Adicionar Comentário"] = "Addere kommentar";
 	$lang["Comentar"] = "Kommentar";
 	$lang["Dar"] = "Give";
 	$lang["para o produto"] = "for produkt";
 	$lang["Cancelar"] = "Aflyse";
 	$lang["Avaliar"] = "Vurdere";

 	// sobre.php
	$lang["historia"] = "historiaDn"; // Não mecher
 	$lang["missao"] = "missaoDn"; // Não mecher
 	$lang["visao"] = "visaoDn"; // Não mecher
 	$lang["valores"] = "valoresDn"; // Não mecher 
?>