<?php

	if (empty($_SESSION['id'])) {session_start();}

	session_destroy();
	setcookie('email', "...", time()-1);
	setcookie('nome', "...", time()-1);
	setcookie('tipo', "...", time()-1);

	header("Location: index.php");
	exit;

?>