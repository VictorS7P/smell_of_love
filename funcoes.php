<?php
	include 'conexao.php';

	// Verifica se os checkboxes foram checados, atribuindo s/n neles!
	function validaCheck($checkbox) {
		if (empty($_POST[$checkbox])) {
			$checkbox = 'N';
		} else {
			$checkbox = 'S';
		}
		return $checkbox;
	}

	// Valida os inputs!
	function valida_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);

		if ($data == "") {
			$data = null;
		}

		return $data;
	}

	// Retorna valores muito utilizados no site
	$sql = "SELECT * FROM smell Where id=1 Limit 1";
	$res = $con->query($sql);
	if ($res) {
		while ($linha = $res->fetch_assoc()) {
			$emailSite = $linha['email'];
			$email2Site = $linha['email2'];
			$telefoneSite = $linha['telefone'];
			$telefone2Site = $linha['telefone2'];
			$celularSite = $linha['celular'];
			$linkFacebookSite = valida_link($linha['linkFacebook']);
			$linkInstagramSite = valida_link($linha['linkInstagram']);
			$linkSnapchatSite = valida_link($linha['linkSnapchat']);
			$ruaSite = $linha['rua'];
			$numeroSite = $linha['numero'];
			$bairroSite = $linha['bairro'];
			$cepSite = $linha['cep'];
			$cidadeSite = $linha['cidade'];
		}
	}

	// Valida os links do BD
	function valida_link($link) {
		if (!(preg_match('/^(https:\/\/)/', $link))) {
			$link = "https://$link";
		}

		return $link;
	}

	// Seta os Headers de HTML para as páginas de usuários conforme o login
	function setarHeader() {
		if (isset($_SESSION['nome'])) {
			include 'headerLogado.php';
		} else {
			include 'header.php';
		}
	}

	// Inicia e carrega sessão do usuário
	function carregarSessao() {
		if (!(isset($_SESSION))) {session_start();}
		
		if (isset($_COOKIE['email'])) {
			$_SESSION['nome'] = $_COOKIE['nome'];
			$_SESSION['tipo'] = $_COOKIE['tipo'];
		}

		setcookie('ultimaPagina', $_SERVER["PHP_SELF"]);
	}

	// Evitar palavrões nos comentários
	function corrigir($comentario) {
		$palavroes = array(
			'arrombado',
			'arrombada',
			'arombado',
			'arombada',
			'buceta',
			'boceta',
			'bucetao',
			'bocetao',
			'bucetaum',
			'bocetaum',
			'bucetinha',
			'bocetinha',
			'blowjob',
			'#@?$%~',
			'caralinho',
			'caralhao',
			'caralhaum',
			'caralhex',
			'cacete',
			'cacetinho',
			'cacetao',
			'cacetaum',
			'epenis',
			'ehpenis',
			'penis',
			'pênis',
			'c*',
			'c*',
			'c*',
			'cú',
			'cuzinho',
			'cúzinho',
			'cuzão',
			'cúzao',
			'cuzudo',
			'cúzudo',
			'cusinho',
			'cúsinho',
			'cúsão',
			'cusão',
			'cúsao',
			'cusao',
			'cusudo',
			'cúsudo',
			'foder',
			'f****',
			'fodase',
			'f***-se',
			'fodasse',
			'f***-sse',
			'fodasi',
			'f***-si',
			'fodassi',
			'f***-ssi',
			'fodassa',
			'f***ça',
			'fodinha',
			'fodao',
			'fodaum',
			'f***',
			'fodona',
			'f***',
			'foder',
			'f****',
			'fodeu',
			'fuckoff',
			'fuckyou',
			'fuck',
			'filhodaputa',
			'filho-da-#@?$%~',
			'fdp',
			'filhadaputa',
			'filha-da-#@?$%~',
			'filho de uma egua',
			'filho de uma égua',
			'filho-de-uma-egua',
			'filho-de-uma-égua',
			'filhodeumaegua',
			'filhodeumaégua',
			'filha de uma egua',
			'filha de uma égua',
			'filha-de-uma-egua',
			'filha-de-uma-égua',
			'filhadeumaegua',
			'filhadeumaégua',
			'gozo',
			'goza',
			'gozar',
			'gozada',
			'gozadanacara',
			'm*****',
			'merdao',
			'merdaum',
			'merdinha',
			'vadia',
			'vasefoder',
			'venhasefoder',
			'voufoder',
			'vasefuder',
			'venhasefuder',
			'voufuder',
			'vaisefoder',
			'vaisefuder',
			'venhasefuder',
			'vaisifude',
			'v****',
			'vaisifuder',
			'vasifuder',
			'vasefuder',
			'vasefoder',
			'palavrãoteste',
			'pirigueti',
			'piriguete',
			'p****',
			'p****',
			'porraloca',
			'porraloka',
			'porranacara',
			'#@?$%~',
			'putinha',
			'putona',
			'putassa',
			'putao',
			'punheta',
			'putamerda',
			'putaquepariu',
			'putaquemepariu',
			'putaquetepariu',
			'putavadia',
			'pqp',
			'putaqpariu',
			'putaqpario',
			'putaqparil',
			'peido',
			'peidar',
			'porra',
			'xoxota',
			'xota',
			'xoxotinha',
			'xoxotona'
		);

		$palavras = explode(" ", $comentario);
		$n_palavras = count($palavras);

		for ($i=0; $i < $n_palavras; $i++) {
			if(in_array($palavras[$i], $palavroes)) {
				$palavras[$i] = str_replace($palavroes, '****', $palavras[$i]);
			}
		}

		$comentario = implode(" ", $palavras);
		return $comentario;
	}

	// Função para os radio boxes da página de perfil
	function checarRadios($dataBanco, $dataHTML) {
		if ($dataBanco == $dataHTML) {
			echo "checked='checked'";
		}
	}
 ?>