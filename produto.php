<?php 
include 'head.php';
?>
	<title>Smell of Love | <?php echo $lang["Produto"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			setarHeader();

			if (empty($_GET['produto'])) {
				echo "<script>window.location.href = 'index.php'</script>";
			}

			$id = $_GET['produto'];

			if (isset($_SESSION['id'])) {
				$sql = "SELECT * FROM avaliacoes WHERE id_produto = '$id' AND id_avaliador = '{$_SESSION['id']}'";
			} else {
				$sql = "SELECT * FROM avaliacoes WHERE id_produto = '$id'";
			}
			
			$res = $con->query($sql);

			if ($res) {
				while ($linha = $res->fetch_assoc()) {
					$avaliacaoAtualUnica = $linha['avaliacao'];
				}	
			} 
			
		?>
			<section>
				<div class="col-12 col-md-8 offset-md-2 my-3">
					<div class="row">
					<?php 
						$sql = "SELECT * FROM produtos WHERE id = '$id' LIMIT 1";

						$res = $con->query($sql);

						if ($res) {
							if ($res->num_rows == 0) {
								echo "<script>window.location.href = 'index.php'</script>";
							}

							while ($linha = $res->fetch_assoc()) {
					?>
						<div class="col-12 col-md-5 img-produto align-self-center pb-2 pb-md-0">
							<img src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="" class="img-fluid d-block mx-auto">
						</div>
						<div class="col-12 col-md-7 informacoes-produto">
							<div class="jumbotron item-sobre py-1 rounded-0">
								<hr class="jumbotron-hr my-1">
								<h2 class="display-4 text-center pb-0 mb-0"><?php echo $linha['nome']; ?></h2>
								<p class="indent-0 text-center">
									<small class="fonte-normal"><?php echo $linha['conteudo']; ?>ml</small>
									<br>
									<small class="fonte-normal"><?php echo $linha['estoque']; ?> <?php echo $lang["em estoque"]; ?></small>
									<br>
									<small class="fonte-normal"><?php echo $linha['quantidadeVendas']; ?> <?php echo $lang["vendidos"]; ?></small>
								</p>
								<span class="fonte-normal" style="color: white">
									<span id="avaliacao" data-toggle="modal" data-target="#modalAvaliacao" style="color:white" class="mx-0">
									<?php 
					    				for ($i=0;$i<$linha['avaliacaoMedia'];$i++) {
					    					echo "
												<span class='estrela fa fa-star' onclick='avaliarProduto(" . ($i + 1) . ")'></span>														
											";
										}

										for ($i=$linha['avaliacaoMedia'];$i<5;$i++) {
					    					echo "
												<span class='estrela fa fa-star-o' onclick='avaliarProduto(" . ($i + 1) . ")'></span>														
											";
										}										
									?>
									&nbsp;									
									</span>
									<?php echo $linha['avaliacaoMedia'] . "/5"; ?>									
									<span class="fonte-normal mx-1">
										<?php
											if (isset($avaliacaoAtualUnica)) {
												echo "- {$lang["Sua avaliação atual"]}: " .  $avaliacaoAtualUnica . "/5";
											} else {
												echo "- {$lang["Sua avaliação atual"]}: {$lang["Não avaliado ainda"]}";
											}

										?>
									</span>
								</span>
								<hr class="jumbotron-hr my-1">
								<p class="fonte-normal lead text-justify">
									<?php echo $linha[$lang["descricao"]]; ?>
								</p>
								<button value="<?php echo $linha['id']; ?>" class="btn btn-padrao rounded-0 d-block mx-auto" onclick="adicionarCarrinho(this)"><?php echo $lang["Adicionar ao carrinho"]; ?></button>
								<hr class="jumbotron-hr">
							</div>
						</div>
					<?php 
							}
						}							
					?>
					</div>
					<div class="row">
						<div class="col-12" id="comentarios">
						<hr>
						<h6 class="display-4"><span class="fa fa-comment"></span> <?php echo $lang["Comentários"]; ?></h6>
					<?php 
						$sql = "SELECT * FROM comentarios WHERE id_produto = '$id'";

						$res = $con->query($sql);

						if ($res) {
							while ($linha = $res->fetch_assoc()) {
					?>		
							<blockquote class="blockquote comentario my-1" id="comentario-<?php echo $linha['id']; ?>">								
								<p class="mb-0 fonte-normal lead">
									<?php
										echo $linha['comentario'];
										if (isset($_SESSION['id'])) {
											if ($linha['id_autor'] == $_SESSION['id'] || $_SESSION['tipo'] == 2) {
									?>									
									<button class="btn btn-danger btn-sm float-right" value="<?php echo $linha['id']; ?>" onclick='excluirComentario(this)'><span class="fa fa-remove"></span></button>
									<?php
											}
										}
									?>
								</p>
								<footer class="blockquote-footer fonte-normal"><?php echo $linha['autor']; ?></footer>
							</blockquote>							
					<?php
							}
							if ($res->num_rows < 1)	{
					?>
						<blockquote class="blockquote comentario my-1">
							<p class="mb-0 fonte-normal lead"><?php echo $lang["Nenhum comentário até o momento"]; ?></p>
						</blockquote>					
					<?php
							}
						}

						if (isset($_SESSION['nome'])) {							
					?>
						<button class="btn btn-success btn-padrao rounded-0 my-0" data-toggle="modal" data-target="#comentar"><?php echo $lang["Adicionar Comentário"]; ?></button>
					<?php
						}
					?>
						</div>
					<div id="comentar" class="modal fade" role="dialog">
						<div class="modal-dialog modal-lg">
							<div class="modal-content rounded-0">
								<div class="modal-header justify-content-center py-0">
									<div class="modal-title display-4 py-0 my-1"><?php echo $lang["Comentar"]; ?></div>
								</div>
								<div class="modal-body">
									<div class="justify-content-center" id="novoComentario">
										<textarea class="form-control rounded-0 fonte-normal" name="comentario" id="comentario"></textarea>
										<button class="btn btn-success btn-padrao my-1 rounded-0 float-right" id="comentar" name="comentar" value="<?php echo $id; ?>" onclick="comentar(this)"><?php echo $lang["Comentar"]; ?></button>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="modal fade" id="modalAvaliacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				 	<div class="modal-dialog" role="document">
				 		<div class="modal-content">						
							<div class="modal-body fonte-normal">
								<?php echo $lang["Dar"]; ?> <span id="quantidadeEstrelas" name="quantidadeEstrelas" class="fonte-normal"></span> <?php echo $lang["para o produto"]; ?>?
							</div>
				
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $lang["Cancelar"]; ?></button>
								<button type="button" class="btn btn-primary btn-padrao" onclick="mandarAvaliacao(this)" value="<?php echo $id; ?>"><?php echo $lang["Avaliar"]; ?></button>
							</div>
						</div>
					</div>
				</div>
			</section>
		<?php include 'footer.php'; ?>
</html>