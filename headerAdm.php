<?php
	if (isset($_COOKIE['idioma'])) {
		if ($_COOKIE['idioma'] == "en") {
			include 'en.php';
		} else if ($_COOKIE['idioma'] == "dn") {
			include 'dn.php';
		}
	} else {
		include 'pt-br.php';
	}
?>

<header>
			<div id="barraHeader" class="col-md-12 col-sm-12 col-12">
				<div class="row barra-topo nodropped rounded-0 align-items-center">					
					<div id="barraLogin" class="col-12 btn-topo no-gutters pr-0 pl-0 align-self-baseline mb-1 mb-md-0 text-center">
						<span class="mb-0 fonte-normal text-center d-inline"><?php echo $lang["Área do administrador"]; ?></span>
						<a href="logout.php" value="logout" class="btn btn-sm btn-default rounded-0 ml-1 d-inline">
							<span class="botao-user p-0 mb-0"><i class="fa fa-sign-out rounded-0" aria-hidden="true"></i><?php echo $lang["Sair"]; ?></span>
						</a>
						<ul class="list-inline d-inline">
							<li class="list-inline-item">
								<button class="p-0 m-0 sem-fundo" onclick="traduzir('pt')">
									<img src="_assets/imagens/idiomas/pt.png" alt="Pt-Br">
								</button>
							</li>								

							<li class="list-inline-item">
								<button class="p-0 m-0 sem-fundo" onclick="traduzir('en')">
									<img src="_assets/imagens/idiomas/in.png" alt="En">
								</button>
							</li>

							<li class="list-inline-item">
								<button class="p-0 m-0 sem-fundo" onclick="traduzir('dn')">
									<img src="_assets/imagens/idiomas/dn.png" alt="Dn">
								</button>
							</li>								
						</ul>				
					</div>	
				</div>				
			</div>
								
			<div class="col-12">
				<div class="row clearfix logo-pesquisa">
					<div class="col-md-8 offset-md-2 logo pt-1 pb-1 col-11 offset-12">
						<a href="administrador.php">
							<img src="_assets/imagens/logo-b.png" class="img-fluid" alt="Smell of Love" />
						</a>
					</div>

					<div class="col-1 offset-md-1 align-self-center">
					<button id="mostrar" value="header" class="btn-header btn btn-sm rounded-0 border-0 float-right">
							<i class="fa fa-chevron-down" aria-hidden="true"></i>
					</button>
					</div>
				</div>
			</div>		
		</header>
  </div>
</div>