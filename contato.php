<?php 
	include 'head.php';	
?>
	<title>Smell of Love | <?php echo $lang["Contato"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			setarHeader();
		?>
		<section>
			<div class="col-12 col-md-8 offset-md-2 pl-md-0 my-3">
				<div class="row contato align-items-center justify-content-center">
					<div class="mb-3 col-12">
						<div class="form-control rounded-0 item-sobre">
							<h2 class="display-4 fonte-normal indent-0 text-center"><?php echo $lang["Fale Conosco"]; ?></h2>
							<hr class="jumbotron-hr my-1">
							<div class="form-group row">
								<label for="nomeContato" class="col-6 col-md-1 pr-md-1 col-form-label fonte-normal text-md-right">
									<?php echo $lang["Nome"]; ?>*
								</label>								
								<div class="col-12 col-md-3 px-md-1">
									<input type="text" class="form-control rounded-0 fonte-normal" name="nomeContato" id="nomeContato" required="required" value="<?php if (isset($_SESSION['nome'])) {echo $_SESSION['nome'];} ?>">
								</div>
								
								<label for="emailContato" class="col-6 col-md-1 px-md-1 col-form-label fonte-normal text-md-right">
									<?php echo $lang["e-mail"]; ?>*
								</label>
								<div class="col-12 col-md-3 px-md-1">
									<input type="e-mail" class="form-control rounded-0 fonte-normal" name="emailContato" id="emailContato" required="required" value="<?php if (isset($_SESSION['nome'])) {echo $_SESSION['email'];} ?>">
								</div>

								<label for="telContatoContato" class="col-6 col-md-1 px-md-1 col-form-label fonte-normal text-md-right">
									<?php echo $lang["Telefone"]; ?>*
								</label>
								<div class="col-12 col-md-3 pl-md-2">
									<input type="text" class="form-control rounded-0 fonte-normal ml-md-1" name="telContatoContato" id="telContatoContato" required="required" placeholder="(__) _____-____" onkeyup="verificarTel('telContatoContato')" value="<?php if (isset($_SESSION['nome'])) {echo $_SESSION['tel'];} ?>">
								</div>

								<label for="mensagem" class="col-6 col-md-1 pr-1 col-form-label fonte-normal text-md-right">
									<?php echo $lang["Mensagem"]; ?>
								</label>
								<div class="col-12">
									<textarea  class="form-control fonte-normal rounded-0" name="mensagem" id="mensagem" required="required"></textarea>
								</div>

								<div class="col-1">
									<button type="submit" class="btn btn-secondary fonte-normal mt-2 rounded-0" onclick="mandarMensagem()"><?php echo $lang["Enviar"]; ?></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row contato align-items-center">
					<div class="col-12 col-sm-9 mapa">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1828.1507989049423!2d-46.67610204373178!3d-23.593514184666958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5756d67c3cd9%3A0x2569f16336ae5159!2sR.+Jo%C3%A3o+Cachoeira%2C+1508+-+Vila+Nova+Concei%C3%A7%C3%A3o%2C+S%C3%A3o+Paulo+-+SP%2C+04535-007!5e0!3m2!1spt-BR!2sbr!4v1492298234529" width="100%" height="300" frameborder="1" style="border:1px solid black" allowfullscreen></iframe>
					</div>
					<div class="col-12 col-sm-3">
						<div class="jumbotron py-1 my-0 item-sobre rounded-0">
							<hr class="jumbotron-hr my-1">
							<p class="text-center fonte-normal lead my-1" style="text-indent: 0px;">
								<?php
									echo "	$ruaSite <br>
											{$lang["Nº"]} $numeroSite <br>
											$bairroSite <br>
											$cepSite <br>
											$cidadeSite <br>";
								?>										
							</p>
							<hr class="jumbotron-hr my-1">
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php 
			include 'footer.php';
			if (isset($_SESSION['nome'])) {
				echo "	<script>
							bloquearInputs();
						</script>";
			}
		?>
</html>