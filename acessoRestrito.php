<?php
	if (!isset($_SESSION)) { session_start(); }

	if (empty($_SESSION['tipo'])) {
		session_destroy();
		header("Location: logout.php");
		exit;
	}

	if ($_SESSION['tipo'] != 2) {
		session_destroy();
		header("Location: logout.php");
		exit;
	}
?>