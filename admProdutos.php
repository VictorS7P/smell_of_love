<?php 
	include 'head.php';
	include 'acessoRestrito.php';

	if (isset($_COOKIE['erro'])) {
		$erro = $_COOKIE['erro'];

		if ($erro == 'dimensao') {
			echo("	<script>
						alert('{$lang['Imagem nas dimensões erradas... Cadastre somente imagens 400x200...']}');								
					</script>");
		}

		setcookie('erro', '...', time()-1);
	}
?>
	<title>Smell of Love | <?php echo $lang["Produtos"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php include 'headerAdm.php'; ?>
		<section>
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row">				
					<div class="col-12">
						<button class="py-3 my-0 btn btn-padrao btn-block my-2" data-toggle="modal" data-target="#novo"><?php echo $lang["Cadastrar Produto"]; ?></button>						
					</div>
				</div>
				<div class="row">
					<div class="col-12 px-0">						
						<h2 class="display-4 text-center fonte-normal">
							<a href="admProdutos.php?tipo=M" class="btn btn-sm btn-masculino"><?php echo $lang["Masculinos"]; ?></a>
							<?php echo $lang["Produtos Cadastrados"]; ?>
							<a href="admProdutos.php?tipo=F" class="btn btn-sm btn-feminino"><?php echo $lang["Femininos"]; ?></a>
						</h2>
						<button class="btn btn-sm btn-secondary float-right m-2 <?php if(isset($_GET['modo'])) {echo 'rotated';} ?>" onclick="<?php if(isset($_GET['modo'])) {echo 'des';} ?>rodar(this)">
							<span class="fa fa-arrow-up"></span>
						</button>	
						<select name="ordenar" id="ordenar" class="custom-select float-right my-2 mx-0" onchange="ordenarProdutos(this.value)">
							<option class="fonte-normal" value="-1"><?php echo $lang["Ordenar Por"]; ?></option>
							<option class="fonte-normal" value="0"><?php echo $lang["Nada"]; ?></option>
							<option class="fonte-normal" value="1"><?php echo $lang["Estoque"]; ?></option>
							<option class="fonte-normal" value="2"><?php echo $lang["Quantidade de vendas"]; ?></option>
							<option class="fonte-normal" value="3"><?php echo $lang["Avaliação"]; ?></option>
							<option class="fonte-normal" value="4"><?php echo $lang["Preço"]; ?></option>
						</select>									
					</div>
				</div>
				<div class="row">
					<?php
						 $sql = "SELECT * FROM produtos";

						 if (isset($_GET['tipo'])) {
						 	echo "	<div class='col-12'>
						 				<div class='row justify-content-center'>
						 					<a href='admProdutos.php' class='btn btn-sm btn-padrao roundedo-0 w-50 py-2 my-1'>{$lang["Ver Todos"]}</a>
						 				</div>
						 			</div>";
						 	$tipo = $_GET['tipo'];
						 	$sql .= " WHERE tipo = '$tipo'";						 			 				 
						 }

						 if (isset($_GET['querie'])) {
						 	$ordem = $_GET['querie'];
						 	$sql .= " ORDER BY $ordem";

						 	if(isset($_GET['modo'])) {
						 		$sql .= " DESC";
						 	}
						 }					

						 $res = $con->query($sql);
						 if ($res) {
							while ($linha = $res->fetch_assoc()) { ?>
							<div class="col-12 col-sm-6 px-0">
								<div id="<?php echo $linha['id'];?>" class="card m-2 rounded-0 card-masculino">
								 	<img src='imagem.php?cod=<?php echo $linha['id']; ?>' class="img-fluid img-produto" />							 		
									<div class="card-header border-0">
							    		<h4 class="card-title my-1 item-produto">
							    			<span class="nome"><?php echo $linha['nome'];?></span>
							    			<span class="float-right">
							    			<?php 
							    				for ($i=0;$i<$linha['avaliacaoMedia'];$i++) {
							    					echo "
														<span class='fa fa-star'></span>														
													";
												}

												for ($i=$linha['avaliacaoMedia'];$i<5;$i++) {
							    					echo "
														<span class='fa fa-star-o'></span>														
													";
												}
											?>
											</span>							    			
							    		</h4>
							    		<small class="tipo fonte-normal">Tipo: <?php $tipo = $linha['tipo']; if ($tipo == 'M') {$tipo = $lang["Masculino"]; } else { $tipo = $lang["Feminino"]; } echo $tipo;?></small>
							    		<br>
							    		<small class="conteudo fonte-normal"><?php echo $linha['conteudo'];?>ml</small>
							    		<br>
							    		<small class="estoque fonte-normal"><?php echo $linha['estoque'];?> <?php echo $lang["em estoque"]; ?></small>
							    		<br>
							    		<small class="fonte-normal"><?php echo $linha['quantidadeVendas'];?> <?php echo $lang["vendidos"]; ?></small>
							    	</div>
									<div class="card-block">					    	
							    		<p class="descricaoPt card-text lead fonte-normal text-justify" style="text-indent: 0"><?php echo $linha["descricao"];?></p>
							    		<hr>
							    		<p class="descricaoEn card-text lead fonte-normal text-justify" style="text-indent: 0"><?php echo $linha["descricaoEn"];?></p>
							    		<hr>
							    		<p class="descricaoDn card-text lead fonte-normal text-justify" style="text-indent: 0"><?php echo $linha["descricaoDn"];?></p>
							   		</div>
							   		<div class="card-footer border-0">
							   			<p class="card-text lead fonte-normal">
							   				<span class="preco">R$ <?php echo $linha['preco'];?></span>
							   				<button value="<?php echo $linha['id'];?>" class="btn btn-padrao float-right rounded-0" data-toggle="modal" data-target="#alterarExcluir" onclick='modalProduto(this.value)'><?php echo $lang["Alterar/Excluir"]; ?></button>
							   			</p>
							   		</div>
							  	</div>
							</div>
					<?php 
							}
						}
						mysqli_close($con);
					?>
				</div>
	
			</div>

		<div id="novo" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header justify-content-center">
						<div class="modal-title titulo-modal">
							<h2><?php echo $lang["Cadastrar novo produto"]; ?></h2>
						</div>
					</div>
					<div class="modal-body">
						<form enctype="multipart/form-data" action="cadastrarProduto.php" method="POST" class="form-control rounded-0 border-0">			
							<div class="form-group ">
								<div class="col-1 fonte-normal pt-1 px-1">
									<?php echo $lang["Nome"]; ?>
								</div>
								<div class="px-1">
									<input type="text" class="mt-1 form-control rounded-0 fonte-normal" name="nomeProduto" id="nomeProduto" required="required" placeholder="<?php echo $lang["Nome do produto..."]; ?>">
								</div>
							</div>	

							<div class="form-group ">
								<div class="col-1 fonte-normal p-1">
									<?php echo $lang["Quantidade"]; ?>
								</div>

								<div class="p-1">
									<div class="input-group item-tabela">
										<input type="button" id="plus" value='-' onclick="carrinho('menos')" class="btn btn-sm rounded-0" />
	  									<input style='width: 50px'  type="number" min="1" name="quantidade" id="quantidadeItens" value="1" class="text-center">	
										<input type="button" id="minus" value='+' onclick="carrinho('mais')" class="btn btn-sm rounded-0">
									</div>
								</div>
							</div>

							<div class="form-group ">
								<div class="fonte-normal p-1">
									<?php echo $lang["Imagem"]; ?>:
								</div>
								<div class="p-1">
									<input type="file" id="imagemProduto" name="imagemProduto" required="required" class="form-control-file">  
									<small class="fonte-normal">(<?php echo $lang["Tamanho"]; ?>: 500x250).</small>
								</div>
							</div>

							<div class="form-group ">
								<div class="fonte-normal p-1">
									<?php echo $lang["Tipo"]; ?>:
								</div>
								<div class="p-1">
									<div class="form-check form-check-inline">
										<label for="tipoM" class="form-check-label fonte-normal custom-control custom-radio">
											<input type="radio" class="form-check-input custom-control-input" name="tipo" id="tipoM" value="M" required="required" onclick="validar(this)">
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description fonte-normal"><?php echo $lang["Masculino"]; ?></span>
										</label>
									</div>
									<div class="form-check form-check-inline">
										<label for="tipoF" class="form-check-label fonte-normal custom-control custom-radio">
											<input type="radio" class="form-check-input custom-control-input bg-danger" name="tipo" id="tipoF" value="F" onclick="validar(this)">
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description fonte-normal"><?php echo $lang["Feminino"]; ?></span>
										</label>
									</div>
								</div>
							</div>

							<div class="form-group ">
								<div class="col-1 fonte-normal p-1">
									<?php echo $lang["Descrição"]; ?>
								</div>
								<div class="px-1">
									<textarea class="mt-1 form-control rounded-0 fonte-normal" name="descricaoProdutoPt" id="descricaoProdutoPt" required="required" placeholder="<?php echo $lang["Descrição em português"]; ?>"></textarea>
								</div>
								<div class="px-1">
									<textarea class="mt-1 form-control rounded-0 fonte-normal" name="descricaoProdutoEn" id="descricaoProdutoEn" required="required" placeholder="<?php echo $lang["Descrição em inglês"]; ?>"></textarea>
								</div>
								<div class="px-1">
									<textarea class="mt-1 form-control rounded-0 fonte-normal" name="descricaoProdutoDn" id="descricaoProdutoDn" required="required" placeholder="<?php echo $lang["Descrição em dinamarquês"]; ?>"></textarea>
								</div>
							</div>

							<div class="form-group ">
								<div>
									<div>
										<div class="fonte-normal px-1">
											<?php echo $lang["Conteúdo do frasco"]; ?> (em ml)
										</div>
								
										<div class="px-1">
											<input type="number" class="mt-1 form-control rounded-0 fonte-normal" name="conteudoProduto" id="conteudoProduto" required="required">
										</div>
									</div>
								</div>
								<div>
									<div>
										<div class="fonte-normal px-1 mt-2 mt-md-0">
											<?php echo $lang["Preço"]; ?> (em R$)
										</div>

										<div class="px-1">
											<input type="text" class="mt-1 form-control rounded-0 fonte-normal" name="precoProduto" id="precoProduto" required="required" placeholder="00,00">
										</div>
									</div>
								</div>
							</div>
							<div class=" justify-content-start px-1">
								<button type="submit" class="btn btn-success rounded-0"><?php echo $lang["Cadastrar"]; ?></button>
							</div>
						</form>
					</div>				
				</div>
			</div>
		</div>

		<div id="alterarExcluir" class="modal fade" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header justify-content-center">
						<div class="modal-title titulo-modal">
							<h2>
								<?php echo $lang["Alterar/Excluir produto"]; ?>
							</h2>
						</div>
					</div>
					<div class="modal-body">
						<form id="formularioNovoProduto" method="POST" enctype="multipart/form-data">
							<div class="col-12 px-0">
								<div id="carModalProduto" class="card m-2 rounded-0 card-masculino">
							 		<img class="card-img-top w-100 img-fluid img-produto rounded-0" src='imagem.php?cod=' alt="Card image cap">
							 		<input type="file" id="novaImagemProduto" name="novaImagemProduto" required="required" class="form-control-file py-1">  
									<small class="fonte-normal">(<?php echo $lang["Tamanho"]; ?>: 500x500).</small>
									<div class="card-header border-0">
							    		<h4 class="card-title my-1 item-produto">
							    			<?php echo $lang["Nome"]; ?>:
							    			<input type="text" class="fonte-normal px-1" placeholder="<?php echo $lang["Nome"]; ?>" id="novoNomeProduto" name="novoNomeProduto" required="required">
							    		</h4>
							    		<small class="fonte-normal">
							    			<?php echo $lang["Conteúdo do frasco"]; ?>:
							    			<input type="number" class="fonte-normal px-1" placeholder="<?php echo $lang["Somente números"]; ?>" id="novoConteudoProduto" name="novoConteudoProduto" required="required">
							    			ml's
							    		</small>
							    		<br>
							    		<small class="fonte-normal">
							    			<?php echo $lang["Tipo"]; ?>:
					    					<div class="form-check form-check-inline">
												<label for="novoTipoM" class="form-check-label fonte-normal custom-control custom-radio">
													<input type="radio" class="form-check-input custom-control-input" name="novoTipo" id="novoTipoM" value="M" required="required" onclick="validar(this)">
													<span class="custom-control-indicator"></span>
													<span class="custom-control-description fonte-normal"><?php echo $lang["Masculino"]; ?></span>
												</label>
											</div>
											<div class="form-check form-check-inline">
												<label for="novoTipoF" class="form-check-label fonte-normal custom-control custom-radio">
													<input type="radio" class="form-check-input custom-control-input bg-danger" name="novoTipo" id="novoTipoF" value="F" onclick="validar(this)">
													<span class="custom-control-indicator"></span>
													<span class="custom-control-description fonte-normal"><?php echo $lang["Feminino"]; ?></span>
												</label>
											</div>		    				
							    			</small>
							    		<br>
							    		<small class="fonte-normal">
							    			<?php echo $lang["Estoque"]; ?>:
											<input type="number" class="fonte-normal px-1" placeholder="<?php echo $lang["Somente números"]; ?>" id="novoEstoqueProduto" name="novoEstoqueProduto" required="required">
							    		</small>		    		
							    	</div>
									<div class="card-block">					    	
							    		<p class="card-text lead fonte-normal text-justify" style="text-indent: 0">
							    			<?php echo $lang["Descrição em português"]; ?>:
											<textarea class="mt-1 form-control rounded-0 fonte-normal" name="novaDescricaoProdutoPt" id="novaDescricaoProdutoPt" required="required"></textarea>
							    		</p>
							    		<hr>
							    		<p class="card-text lead fonte-normal text-justify" style="text-indent: 0">
							    			<?php echo $lang["Descrição em inglês"]; ?>:
							    			<textarea class="mt-1 form-control rounded-0 fonte-normal" name="novaDescricaoProdutoEn" id="novaDescricaoProdutoEn" required="required"></textarea>
							    		</p>
							    		<hr>
							    		<p class="card-text lead fonte-normal text-justify" style="text-indent: 0">
							    			<?php echo $lang["Descrição em dinamarquês"]; ?>:
							    			<textarea class="mt-1 form-control rounded-0 fonte-normal" name="novaDescricaoProdutoDn" id="novaDescricaoProdutoDn" required="required"></textarea>
							    		</p>
							   		</div>
							   		<div class="card-footer border-0">
							   			<p class="card-text lead fonte-normal">
							   				<?php echo $lang["Preço"]; ?>:
							   				<input type="text" class="w-25 form-control rounded-0 fonte-normal" name="novoPrecoProduto" id="novoPrecoProduto" required="required" placeholder="00,00">							   				
							   			</p>
							   			<button type="submit" class="btn btn-success btn-padrao rounded-0" name="alterarProduto" id="alterarProduto"><?php echo $lang["Alterar"]; ?></button>
							   			<a style='color: white' class="btn btn-danger bg-danger border-0 rounded-0 fonte-normal" name="excluirProduto" id="excluirProduto"><?php echo $lang["Excluir"]; ?></a>
							   		</div>
							   	</div>
						  	</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		</section>
		<?php include 'footer.php'; ?>
</html>