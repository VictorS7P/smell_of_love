<?php 
	// admCupons.php
	$lang["cupons de desconto"] = "Discount coupons";
	$lang["cadastrar novo cupom"] = "Sign up for a new coupon";
	$lang["Código"] = "Code";
	$lang["Valor Fixo"] = "Fixed Value";
	$lang["Valor Percentual"] = "Percentage Value";
	$lang["Cadastrar"] = "Register";
	$lang["O código deve conter 8 números"] = "The code must contain 8 numbers";
	$lang["Cupons cadastrados"] = "Registered Coupons";
	$lang["Descrição"] = "Description";
	$lang["Deletar"] = "Delete";
	$lang["descricao"] = "descricaoEn";

	// admInfos.php
	$lang["Informações Gerais"] = "General Information";
	$lang["Alterar informações gerais do site"] = "Change general site information";
	$lang["Item"] = "Item";
	$lang["Valor atual"] = "Current Value";
	$lang["Alterar"] = "Change";
	$lang["e-mail"] = "e-mail";
	$lang["2º e-mail"] = "2nd e-mail";
	$lang["Telefone"] = "Phone";
	$lang["2º telefone"] = "2nd phone";
	$lang["Celular"] = "Mobile";
	$lang["Link página Facebook"] = "Link Facebook page";
	$lang["Link Instagram"] = "Link Instagram";
	$lang["Snapchat"] = "Snapchat";
	$lang["Rua"] = "Street";
	$lang["Nº"] = "No.";
	$lang["Bairro"] = "Neighborhood";
	$lang["CEP"] = "CEP";
	$lang["Cidade"] = "City";
	$lang["Alterar Dado"] = "Change Data";
	$lang["Alterar"] = "Change";

	// administrador.php
	$lang["Painel"] = "Panel";
	$lang["Painel Administrativo"] = "Administrative Panel";
	$lang["Acessar Site"] = "Access Site";
	$lang["Alterar/Cadastrar Produtos"] = "Change / Register Products";
	$lang["Alterar Sobre"] = "Change About";
	$lang["Alterar Cupons de Desconto"] = "Change Discount Coupons";
	$lang["Alterar Informações Gerais"] = "Change General Information";
	$lang["novas mensagens"] = "Change General Information";
	$lang["Adicionar Promoção"] = "Add Promotion";

	// admMensagens.php
	$lang["Mensagens"] = "Messages";
	$lang["Arquivadas"] = "Filed";
	$lang["Arquivar"] = "Archive";
	$lang["Responder"] = "Reply";
	$lang["Excluir"] = "Delete";
	$lang["Mensagens Arquivadas"] = "Archived Messages";
	$lang["Recarregue a página para atualizar as mensagens"] = "Reload page to update messages";

	// admProdutos.php
	$lang["Imagem nas dimensões erradas... Cadastre somente imagens 400x200..."] = "Image in the wrong dimensions ... Only record 400x200 images ...";
	$lang["Produtos"] = "Products";
	$lang["Cadastrar Produto"] = "Register Product";
	$lang["Masculinos"] = "Male";
	$lang["Produtos Cadastrados"] = "Registered Product";
	$lang["Femininos"] = "Female";
	$lang["Ordenar Por"] = "Sort By";
	$lang["Nada"] = "Nothing";
	$lang["Estoque"] = "Stock";
	$lang["Quantidade de vendas"] = "Amount of sales";
	$lang["Avaliação"] = "Evaluation";
	$lang["Preço"] = "Price";
	$lang["Ver Todos"] = "View All";
	$lang["Masculino"] = "Male";
	$lang["Feminino"] = "Female";
	$lang["em estoque"] = "in stock";
	$lang["vendidos"] = "sold";
	$lang["Alterar/Excluir"] = "Change/Delete";
	$lang["Cadastrar novo produto"] = "Register new product";
	$lang["Nome"] = "Name";
	$lang["Nome do produto..."] = "Product name...";
	$lang["Quantidade"] = "Quantity";
	$lang["Imagem"] = "Image";
	$lang["Tamanho"] = "Size";
	$lang["Tipo"] = "Type";
	$lang["Descrição"] = "Description";
	$lang["Descrição em português"] = "Description in Portuguese";
	$lang["Descrição em inglês"] = "Description in English";
	$lang["Descrição em dinamarquês"] = "Description in Danish";
	$lang["Conteúdo do frasco (em ml)"] = "Bottle contents (in ml)";
	$lang["Preço (em R$)"] = "Price (in R$)";
	$lang["Cadastrar"] = "Register";
	$lang["Alterar/Excluir produto"] = "Change/Delete Product";
	$lang["Conteúdo do frasco"] = "Bottle contents";
	$lang["Estoque"] = "Stock";
	$lang["Preço"] = "Price";
	$lang["Alterar"] = "Change";
	$lang["Excluir"] = "Delete";
	$lang["Nome"] = "Name";
	$lang["Somente números"] = "Numbers only";

	// admPromocao.php
	$lang["Promoções"] = "Promotions";
	$lang["Produtos em Promoção"] = "Products in Promotion";
	$lang["Sempre que um produto tiver um preço menor definido ele será considerado em promoção"] = "Whenever a product has a lower price set it will be considered for promotion";
	$lang["Conteúdo"] = "Content";
	$lang["Alterar Preço"] = "Change Price";
	$lang["Outros Produtos"] = "Other Products";
	$lang["Novo Preço"] = "New Price";
	$lang["Ok"] = "Ok";

	// admSobre.php
	$lang["Sobre"] = "About";
	$lang["História"] = "History";
	$lang["novaHistoriaPt"] = "Enter a new history(in Portuguese), leave it blank so you do not change the values!";
	$lang["novaHistoriaEn"] = "Enter a new story(in English), leave it blank so you do not change the values!";
	$lang["novaHistoriaDn"] = "Enter a new story (in Danish), leave it blank so you do not change the values!";
	$lang["Missão"] = "Mission";
	$lang["novaMissãoPt"] = "Please enter a new mission(in Portuguese), leave it blank so you do not change the values!";
	$lang["novaMissãoEn"] = "Enter a new mission(in English), leave it blank so you do not change the values!";
	$lang["novaMissãoDn"] = "Please enter a new mission (in Danish), leave it blank so you do not change the values!";
	$lang["Visão"] = "Vision";
	$lang["novaVisãoPt"] = "Enter a new view (in Portuguese), leave it blank so you do not change the values!";
	$lang["novaVisãoEn"] = "Enter a new view, leave it blank so you do not change the values!";
	$lang["novaVisãoDn"] = "Enter a new view (in Danish), leave it blank so you do not change the values!";
	$lang["Valores"] = "Values";
	$lang["novosValoresPt"] = "Enter new values ​​(in Portuguese), leave blank to not change values!";
	$lang["novosValoresEn"] = "Enter new values (in English), leave blank to not change values!";
	$lang["novosValoresDn"] = "Enter new values ​​(in Danish), leave blank to not change values!";
	$lang["AlterarDados"] = "ChangeData";

	// cadastrar.php
	$lang["Cadastrar"] = "Register";
	$lang["Login e Senha"] = "Login and Password";
	$lang["Senha"] = "Password";
	$lang["Confirme sua senha"] = "Confirm your password";
	$lang["Informações Pessoais"] = "Personal Information";
	$lang["Nome completo"] = "ull Name";
	$lang["Sexo"] = "Sex";
	$lang["CPF"] = "CPF";
	$lang["Data de nascimento"] = "Date of birth";
	$lang["Telefone para contato"] = "Contact phone number";
	$lang["Telefone celular"] = "Mobile number";
	$lang["Desejo receber ofertas da Smell por e-mail"] = "I would like to receive Smell offers by email";
	$lang["Desejo receber ofertas da Smell pelo celular"] = "I want to receive Smell offers from my cell phone";
	$lang["Informações para Entrega"] = "Information for Delivery";
	$lang["CEP"] = "CEP";
	$lang["Rua"] = "Street";
	$lang["Complemento"] = "Complement";
	$lang["Bairro"] = "Neighborhood";
	$lang["Cidade"] = "City";

	// checkout.php
	$lang["Checkout"] = "Checkout";
	$lang["Você não tem produtos no carrinho"] = "You have no items in the cart";
	$lang["Minha sacola de compras"] = "My shopping bag";
	$lang["Produto"] = "Product";
	$lang["Valor"] = "Value";
	$lang["Quantidade"] = "Quantity";
	$lang["Total"] = "Total";
	$lang["Excluir"] = "Delete";
	$lang["Subtotal"] = "Subtotal";
	$lang["Desconto"] = "Discont";
	$lang["Frete"] = "Freight";
	$lang["Total"] = "Total";
	$lang["OK"] = "OK";
	$lang["Calcular"] = "Calculate";
	$lang["Finalizar Compra"] = "Finish Purchase";
	$lang["Digite seu cupom de desconto"] = "Enter your discount coupon";
	$lang["Digite seu CEP"] = "Enter your CEP";

	// contato.php
	$lang["Contato"] = "Contact";
	$lang["Fale Conosco"] = "Contact Us";
	$lang["Mensagem"] = "Menssage";
	$lang["Enviar"] = "Send";

	// feminino.php
	$lang["Produtos Femininos"] = "Women's Products";
	$lang["Adicionar ao carrinho"] = "Add to Cart";
	$lang["Nenhum Produto Feminino Cadastrado"] = "No Registered Female Product";

	// footer.php
	$lang["Nossas redes sociais"] = "Our social networks";
	$lang["Fale conosco"] = "Contact Us";
	$lang["Formas de pagamento"] = "Forms of payment";
	$lang["Todos os direitos reservados"] = "All rights reserved";
	$lang["nº"] = "nº";

	// head.php
	$lang["lang"] = "En";

	// header.php
	$lang["Login"] = "Login";
	$lang["Criar uma conta"] = "Create an account";
	$lang["Meu carrinho"] = "My cart";
	$lang["Finalizar"] = " Finish";
	$lang["Pesquisar"] = "Search";
	$lang["Continuar online"] = "Continue online";
	$lang["Logar"] = "Log";
	$lang["E-mail"] = "E-mail";
	$lang["Senha"] = "Password";

	// headerAdm.php
	$lang["Área do administrador"] = "Administrator Area";
	$lang["Sair"] = "Exit";

	// headerLogado.php
	$lang["Perfil"] = "Profile";
	$lang["Administrativo"] = "Administrative";
	$lang["Meu carrinho"] = "My cart";
	$lang["Bem-vindo"] = "Welcome";

	// index.php
	$lang["link-slide1"] = "_assets/imagens/imagem-slide-1en.png";
	$lang["Home"] = "Home";
	$lang["Promoções"] = "Promotions";
	$lang["Últimos Produtos Femininos"] = "Latest Female Products";
	$lang["Ver todos"] = "See All";
	$lang["Últimos Produtos Masculinos"] = "Latest Male Products";

	// masculino.php
	$lang["Produtos Masculinos"] = "Men's Products";
	$lang["Nenhum Produto Masculino Cadastrado"] = "No Registered Male Product";

	// perfil.php
	$lang["Meu Perfil"] = "My Profile";
	$lang["Faça login para continuar"] = "Log in to continue";
	$lang["Meus Dados"] = "My Data";
	$lang["Alterar senha"] = "Change password";
	$lang["Alterar Dados Pessoais"] = "Change Personal Data";
	$lang["Alterar Dados de Entrega"] = "Change Delivery Data";
	$lang["Alterar Senha"] = "Change Password";
	$lang["Digite uma nova senha"] = "Enter a new password";
	$lang["Confirme sua nova senha"] = "Confirm your new password";
	$lang["Digite sua antiga senha"] = "Enter your old password";
	$lang["alterar"] = "change";
	$lang["Alterar Dados"] = "Change Data";
	$lang["Digite sua senha para continuar"] = "Enter your password to continue";
	$lang["alterar"] = "change";

	// pesquisa.php
	$lang["Pesquisa"] = "Search";
	$lang["Resultados da busca para"] = "Search results for";
	$lang["Nenhum Resultado Encontrado"] = "No Results Found";

	// produto.php
	$lang["em estoque"] = "in stock";
	$lang["vendidos"] = "sold";
	$lang["Sua avaliação atual"] = "Your current rating";
 	$lang["Não avaliado ainda"] = "Not yet rated";
 	$lang["Comentários"] = "Comments";
 	$lang["Nenhum comentário até o momento"] = "No comments yet";
 	$lang["Adicionar Comentário"] = "Add Comment";
 	$lang["Comentar"] = "Comment";
 	$lang["Dar"] = "Five";
 	$lang["para o produto"] = "for product";
 	$lang["Cancelar"] = "Cancel";
 	$lang["Avaliar"] = "Evalute";

 	// sobre.php
	$lang["historia"] = "historiaEn";
 	$lang["missao"] = "missaoEn";
 	$lang["visao"] = "visaoEn";
 	$lang["valores"] = "valoresEn";
?>