<?php 
include 'head.php';
?>
	<title>Smell of Love | <?php echo $lang["Checkout"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			setarHeader();

			if (!(isset($_COOKIE['carrinho']))) {
				echo "<script>alert({$lang["Você não tem produtos no carrinho"]}); window.location.href='index.php'</script>";
			} else {
				$carrinho = unserialize($_COOKIE['carrinho']);

				$sql = "SELECT * FROM produtos WHERE id IN (";

				for ($i=0; $i < count($carrinho); $i++) { 
					if ($i == count($carrinho) - 1) {
						$sql .= "$carrinho[$i])";	
					} else {
						$sql .= "$carrinho[$i], ";
					}
				}

				$res = $con->query($sql);							
			}
		?>
		
		<section>
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row">
					<div class="col-12">
						<h3 class="display-4 text-center"><?php echo $lang["Minha sacola de compras"]; ?></h3>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<table class="table table-bordered table-responsive table-hover">
							<thead class="tabela-topo">
								<tr>
									<th class="text-center font-weight-normal w-75"><?php echo $lang["Produto"]; ?></th>
									<th class="text-center font-weight-normal"><?php echo $lang["Valor"]; ?></th>
									<th class="text-center font-weight-normal"><?php echo $lang["Quantidade"]; ?></th>
									<th class="text-center font-weight-normal"><?php echo $lang["Total"]; ?></th>
									<th class="text-center font-weight-normal"><?php echo $lang["Excluir"]; ?></th>
								</tr>
							</thead>
							<tbody>
							<?php
								if ($res) {
									while ($linha = $res->fetch_assoc()) {

							?>
								<tr id="produto-<?php echo $linha['id']; ?>">
									<td class="fonte-normal linha-tabela">
										<div class="item-tabela">
											<img src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="" class="w-25" style="max-height: 50px">
											<p class="lead fonte-normal font-weight-bold">
												<?php echo $linha['nome']; ?>
												<br>
												<small class="fonte-normal"><?php echo $linha['conteudo']; ?>ml</small>
											</p>
										</div>
									</td>
									<td class="text-center align-self-center linha-tabela">
										<p class="fonte-normal item-tabela" id="preco-<?php echo $linha['id']; ?>" style="width: 100px">
											R$ <?php echo $linha['preco']; ?>
										</p>
									</td>
									<td class="linha-tabela">		
										<div class="input-group item-tabela">
											<input type="button" id="plus" value='-' onclick="carrinho('menos', <?php echo $linha['id']. "," . $linha['estoque']; ?>)" class="btn btn-sm rounded-0" />
	  										<input type="number" min="1" name="quantidade<?php echo $linha['id']; ?>" id="quantidade<?php echo $linha['id']; ?>" value="1" class="w-50 text-center quantidadeProduto" readonly="readonly">
											<input type="button" id="minus" value='+' onclick="carrinho('mais', <?php echo $linha['id']. "," . $linha['estoque']; ?>)" class="btn btn-sm rounded-0">
										</div>								
									</td>
									<td class="text-center align-self-center linha-tabela">
										<p class="preco-total fonte-normal item-tabela" id="valor-total-<?php echo $linha['id']; ?>" style="width: 100px">
											R$ <?php echo $linha['preco']; ?>
										</p>
									</td>
									<td class="text-center table-danger linha-tabela">
										<button value="<?php echo $linha['id']; ?>" class="btn btn-danger btn-sm item-tabela" onclick="removerProduto(this.value)">
											<i class="fa fa-remove"></i>
										</button>
									</td>
								</tr>
								<?php
										}
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row">
					<div class="col-12">
						<div class="jumbotron item-sobre py-1 rounded-0 float-sm-right">
							<ul class="list-unstyled">
								<li class="row justify-content-end mb-2 px-2">
									<p class="my-1">
										<span class="fonte-normal"><?php echo $lang["Subtotal"]; ?>:</span>
										<span class="font-weight-bold" id="subtotal"></span>
									</p>
								</li>
								<li class="row px-2">
									<div class="input-group">
										<input type="text" class="form-control fonte-normal rounded-0" placeholder="<?php echo $lang["Digite seu cupom de desconto"]; ?>" id="cupomPedido">
										<div class="input-group-btn">
											<button class="btn btn-secondary rounded-0" type="button" onclick="validarCupom()">
												<?php echo $lang["OK"]; ?>!
											</button>
										</div>
									</div>
								</li>
								<li class="row justify-content-end mb-2 px-2">
									<p class="my-1">
										<span class="fonte-normal"><?php echo $lang["Desconto"]; ?>:</span>
										<span class="font-weight-bold" id="valorDesconto">R$ 0,00</span>
									</p>
								</li>
								<li class="row px-2">
									<div class="input-group">
										<input type="text" id="cep" class="form-control fonte-normal rounded-0" <?php if(!isset($_SESSION['cep'])) { echo "placeholder='{$lang["Digite seu CEP"]}'"; } else { echo "value='{$_SESSION['cep']}' readonly='readonly'"; } ?>>
										<div class="input-group-btn">
											<button class="btn btn-secondary rounded-0" type="button" onclick="calcularFrete()">
												<?php echo $lang["Calcular"]; ?>
											</button>
										</div>
									</div>
								</li>
								<li class="row justify-content-end px-2">
									<p class="my-1">
										<span class="fonte-normal"><?php echo $lang["Frete"]; ?>:</span>
										<span class="font-weight-bold" id="valorFrete">R$ 0,00</span>
									</p>
								</li>
								<li class="row justify-content-center">
									<p class="my-1" style="font-size: 2em; text-indent: 0px">
										<span class="">Total:</span>
										<span class="font-weight-bold" id="totalPedido"></span>
									</p>
								</li>
								<li class="row justify-content-center">
									<button class="btn btn-lg btn-success rounded-0" type="button" onclick="finalizarCompra()">
										<p class="lead my-0 py-0 font-weight-bold" style="text-indent: 0px;"><?php echo $lang["Finalizar Compra"]; ?></p>
									</button>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>

		<?php include 'footer.php'; ?>

		<script type="text/javascript">
			subtotal = 0;
			var tam = $(".preco-total").length;

			for (var i = 0; i < tam; i++ ) {
				valor = $(".preco-total").eq(i).text();
				subtotal = subtotal + parseFloat(valor.replace(/R\$ /ig, '').replace(/[\,]/g, '.'));
			}

			subtotal = "R$ " + subtotal;
			subtotal = valorExibir(subtotal);
			$("#subtotal").text(subtotal);

			$("#totalPedido").text(subtotal);
		</script>
</html>