<?php
	include "../../funcoes.php";
	carregarSessao();

	if (!(isset($_SESSION['id']))) {
		echo "Você precisa estar logado para finalizar a compra";
		exit;
	} else {
		$idCliente = $_SESSION['id'];
		$cupom = $_GET['cupom'];
		$produtos = unserialize($_COOKIE['carrinho']);

		$carrinho = [];		
		$estoque = [];
		$vendas = [];
		$quantidade = [];		

		sort($produtos);
		for ($i =0; $i < count($produtos); $i++) {
			$quantidade[] = $_GET['quantidade' . $produtos[$i]];
		}

		$sql = "SELECT estoque, quantidadeVendas FROM produtos WHERE ";

		for ($i = 0; $i < count($produtos); $i++) {
			$sql .= "id = " . $produtos[$i];
			if ($i + 1 < count($produtos)) {
				$sql .= " OR ";
			}
		}

		$res = $con->query($sql);

		if ($res) {
			while ($linha = $res->fetch_assoc()) {
				$estoque[] = $linha['estoque'];
				$vendas[] = $linha['quantidadeVendas'];
			}

			$carrinho["IDs"] = $produtos;
			$carrinho["comprados"] = $quantidade;
			$carrinho["estoque"] = $estoque;
			$carrinho["vendas"] = $vendas;

			for ($i = 0; $i < count($produtos); $i++) {
				$carrinho["vendas"][$i] = $carrinho["vendas"][$i] + $carrinho["comprados"][$i];
				$carrinho["estoque"][$i] = $carrinho["estoque"][$i] - $carrinho["comprados"][$i];
			}

			$sql = "UPDATE produtos SET estoque = (CASE";

			for ($i = 0; $i < count($produtos); $i++) {
				$sql .= " WHEN id = " . $carrinho["IDs"][$i] . " THEN " . $carrinho["estoque"][$i];
				if ($i + 1 == count($produtos)) {
					$sql .= " END), quantidadeVendas = (CASE";

					for ($a = 0; $a < count($produtos); $a++) {
						$sql .= " WHEN id = " . $carrinho["IDs"][$a] . " THEN " . $carrinho["vendas"][$a];
						if ($a + 1 == count($produtos)) {
							$sql .= " END) WHERE id IN (";
						}
					}

					for ($a = 0; $a < count($produtos); $a++) {
						$sql .= $carrinho["IDs"][$a];
						if ($a + 1 == count($produtos)) {
							$sql .= ")";
						} else {
							$sql .= ", ";
						}
					}
				}				
			}

			$res2 = $con->query($sql);

			if ($res2) {		

				$sql = "";
				for ($i = 0; $i < count($produtos); $i++) {
					$sql .= "DELETE FROM produtos_comprados WHERE id_usuario = {$idCliente} AND id_produto = {$carrinho["IDs"][$i]};";
					$sql .= "INSERT INTO produtos_comprados(id_usuario, id_produto) VALUES({$idCliente}, {$carrinho['IDs'][$i]});";
				}

				$res3 = $con->multi_query($sql);

				if ($res3) {
					setcookie('carrinho', '...', time()-1, '/');
					echo "Pedido finalizado com sucesso!";
					exit;
				} else {
					echo "Erro querie 3";
				}
			}	
			else {
				echo "Erro querie 2";
			}	
		}
	}

?>