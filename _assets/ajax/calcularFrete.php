<?php
	include '../../funcoes.php';
	carregarSessao();
	$cep = $_GET['cep'];
	$quantidade = $_GET['quantidade'];

 $data['nCdEmpresa'] = '';
 $data['sDsSenha'] = '';
 $data['sCepOrigem'] = $cepSite;
 $data['sCepDestino'] = $cep;

// caixa pequena = 9x9x19
// caixa grande = 50x50x50
// largura grande = 5 x pequenas
// altura grande = 5 x pequenas
// comprimento grande = 2 x pequenas
// grande = 50 pequenas

$largura = 9 * $quantidade;
$altura = 9;
$comprimento = 19;
$pilhas = 1;
$filas = 1;
$caixa = [];
// Maximo = 11x11x2

if ($quantidade > 11) {
	$pilhas = ceil($quantidade/11);
	$quantidade = 11;
	
	if ($pilhas > 11) {
		$filas = 2;
		$pilhas = 11;
	}
}

$largura = 9 * $quantidade;
$altura = $pilhas * 9;
$comprimento = 19 * $filas;

if ($quantidade == 1) {
	$largura = 9;
	$altura = 9;
	$comprimento = 19;
}

 $peso = $quantidade * 0.2;
 $data['nVlPeso'] = "$peso";
 $data['nCdFormato'] = '1';
 $data['nVlComprimento'] = $comprimento;
 $data['nVlAltura'] = $altura;
 $data['nVlLargura'] = $largura;
 $data['nVlDiametro'] = '0';
 $data['sCdMaoPropria'] = 'n';
 $data['nVlValorDeclarado'] = '0';
 $data['sCdAvisoRecebimento'] = 'n';
 $data['StrRetorno'] = 'xml';

 $data['nCdServico'] = '40010';
 $data = http_build_query($data);

 $url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';

 $curl = curl_init($url . '?' . $data);
 curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

 $result = curl_exec($curl);
 $result = simplexml_load_string($result);
 foreach($result -> cServico as $row) {
 //Os dados de cada serviço estará aqui
 if($row -> Erro == 0) {
     echo $row -> Valor;
 } else {
     echo $row -> MsgErro;
 }
 }
 exit;
 ?>