<?php
	include '../../funcoes.php';
	carregarSessao();
	$id = $_GET['id'];	

	if (!isset($_COOKIE['carrinho'])) {
		$carrinho = [0 => $id];
		setcookie('carrinho', serialize($carrinho), time()+60*60*24*14, '/');
	} else {
		$carrinho = unserialize($_COOKIE['carrinho']);
		$colocar = false;

		for ($i=0; $i < count($carrinho); $i++) { 
			if (!(in_array($id, $carrinho))) {
				$colocar = true;
			}
		}

		if ($colocar == true) {
			$carrinho[] = $id;
		}

		setcookie('carrinho', serialize($carrinho), time()+60*60*24*14, '/');
	}
	
	echo count($carrinho);
?>