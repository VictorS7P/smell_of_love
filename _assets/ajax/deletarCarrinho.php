<?php
	include '../../funcoes.php';
	carregarSessao();
	$id = $_GET['id'];	
	
	$carrinho = unserialize($_COOKIE['carrinho']);

	if (count($carrinho) <= 1) {

		setcookie('carrinho', '', time()-1, '/');
	} else {

		if(($key = array_search($id, $carrinho)) !== false) {
		    unset($carrinho[$key]);
		}

		if (count($carrinho) > 0) {			
			setcookie('carrinho', serialize($carrinho), time()+60*60*24*14, '/');
		} else {
			setcookie('carrinho', '', time()-1, '/');
		}
	}

	$carrinho = unserialize($_COOKIE['carrinho']);
	echo (count($carrinho) - 1);
?>