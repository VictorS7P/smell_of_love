var Ajax = false;

function AjaxRequest() {
	Ajax = false;
	if (window.XMLHttpRequest) {
		Ajax = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		try {
			Ajax = new ActiveXObject("Msxm12.XMLHTTP");
		} catch (e) {
			try {
				Ajax = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}
}

function buscarBanco(item) {
	var dado = $(item).val();
	var tipo = $(item).attr('name');

	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	}

	Ajax.onreadystatechange = respostaEmail;
	Ajax.open('GET', '_assets/ajax/buscaDados.php?dado='+dado+'&tipo='+tipo, true);
	Ajax.send(null);
	Ajax.close;
}

function respostaEmail(tipo) {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			situacao = Ajax.responseText;

			if (/cadastrado/.test(situacao)) {
				situacao = situacao.replace(/ já cadastrado/ig, "");
				situacao = situacao.replace(/\s/ig, "");
				validaErrado(situacao);
			} 

			if (/válido/.test(situacao)) {
				situacao = situacao.replace(/ válido/ig, "");
				situacao = situacao.replace(/\s/ig, "");
				validaCerto(situacao);
			}

		} else {
			text = document.createTextNode("Erro na resposta do servidor");
			$('#respostaEmail').append(text);
		}
	}
}

function deletarCupom(botao) {
	AjaxRequest();

	var codigo = $(botao).val();

	if(!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		Ajax.onreadystatechange = recarregar;
		Ajax.open('GET', '_assets/ajax/deletarCupom.php?codigo='+codigo, true);
		Ajax.send(null);
		Ajax.close;
	}
}

function recarregar() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			location.reload();
		}
	}
}

function buscarCupons(item) {
	AjaxRequest();

	codigo = $(item).val();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		Ajax.onreadystatechange = buscarCupom;
		Ajax.open('GET', '_assets/ajax/buscarCupom.php?codigo='+codigo, true);
		Ajax.send(null);
		Ajax.close;
	}
}

function buscarCupom() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			situacao = Ajax.responseText;

			if (/uso/ig.test(situacao)) {
				validaErrado("novoCupomCodigo");
				alert("Cupom em uso");
			} else {
				validaCerto("novoCupomCodigo");
			}
		}
	}
}

function mandarMensagem() {
	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		nome = $("#nomeContato").val();
		email = $("#emailContato").val();
		tel = $("#telContatoContato").val();
		msg = $("#mensagem").val();

		Ajax.onreadystatechange = mensagem;
		Ajax.open('GET', '_assets/ajax/mandarMensagem.php?nome='+nome+'&email='+email+'&tel='+tel+'&msg='+msg, true);
		Ajax.send(null);
		Ajax.close;
	}
}

function mensagem() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			alert("Mensagem enviada com sucesso!");
		}
	}
}

function arquivarMensagem(botao) {
	AjaxRequest();

	if (!Ajax) {
		"erro na chamada Ajax";
	} else {
		id = $(botao).val();
		email = null;

		Ajax.onreadystatechange = cinzar;
		Ajax.open('GET', '_assets/ajax/checarMensagem.php?id='+id, true);
		Ajax.send(null);
		Ajax.close;
	}
}

function responderMensagem(botao) {
	AjaxRequest();

	if (!Ajax) {
		"erro na chamada Ajax";
	} else {
		id = $(botao).val();
		email = $("#email-"+id).text();

		Ajax.onreadystatechange = cinzar;
		Ajax.open('GET', '_assets/ajax/checarMensagem.php?id='+id+'&deletar=deletar', true);
		Ajax.send(null);
		Ajax.close;
	}
}

function excluirMensagem(botao) {
	AjaxRequest();

	if (!Ajax) {
		"erro na chamada Ajax";
	} else {
		id = $(botao).val();	
		email = null;

		Ajax.onreadystatechange = cinzar;
		Ajax.open('GET', '_assets/ajax/checarMensagem.php?id='+id+'&deletar=deletar', true);
		Ajax.send(null);
		Ajax.close;
	}
}

function cinzar() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			$("#mensagem-"+id).remove();
			if (email != null) {
				window.location.href = 'mailto:'+email;
			}
		}
	}
}

function promocao(botao) {
	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		id = $(botao).val();
		preco = $(novoPreco).val();

		Ajax.onreadystatechange = respostaPromocao;
		Ajax.open('GET', '_assets/ajax/promocao.php?id='+id+'&preco='+preco, true);
		Ajax.send(null);
		Ajax.close;
	}

}

function respostaPromocao() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			window.location.href = window.location.href;
		}
	}
}

function excluirComentario(botao) {
	AjaxRequest();

	if(!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		id  = $(botao).val();

		Ajax.onreadystatechange = excluirComentarioPagina;
		Ajax.open('GET', '_assets/ajax/excluirComentario.php?id='+id, true);
		Ajax.send(null);
		Ajax.close;
	}
}

function excluirComentarioPagina() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			$('#comentario-'+id).remove();
		}
	}
}

function comentar(botao) {
	AjaxRequest();

	if(!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		comentario = $("#comentario").val();
		id = $(botao).val();

		Ajax.onreadystatechange = mostrarComentario;
		Ajax.open('GET', '_assets/ajax/comentar.php?comentario='+comentario+'&prod='+id, true);
		Ajax.send(null);
		Ajax.close;
	}
}

function mostrarComentario() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			// id = Ajax.responseText;
			// $("#comentar").hide();

			// $("#comentarios").createElement('blockquote[id="#bloq"]');
			// $('#bloq').addClass('blockquote comentario my-1').attr('id', 'comentario-'+id);

			// p = createElement('p');
			// $(p).addClass('mb-0 fonte-normal lead');
			// $(p).text(comentario);

			// footer = createElement('footer');
			// $(footer).addClass('blockquote-footer fonte-normal');
			// $(footer).text('Autor');

			// $(bloq).append($(p)).append($(footer));	

			window.location.href = window.location.href;
		}
	}
}

function mandarAvaliacao(botao) {
	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		produto = $(botao).val();
		avaliacao = $("#quantidadeEstrelas").text();
		
		Ajax.onreadystatechange = respostaAvaliacao;
		Ajax.open('GET', '_assets/ajax/avaliarProduto.php?avaliacao='+avaliacao+'&produto='+produto, true);
		Ajax.send(null);
		Ajax.close;	
	}
}

function respostaAvaliacao() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			novaAvaliacao = Ajax.responseText;
			alert(novaAvaliacao);
			location.reload();
		}
	}
}

function alterarDadoUser(opcao) {
	// 1 = Senha
	// 2 = Pessoais

	AjaxRequest();
	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		switch (opcao) {
			case 1:
				novaSenha = $('#senha').val();
				confirmaSenha = $('#confirmaSenha').val();
				antigaSenha = $('#antigaSenhaModal').val();


				if (novaSenha != confirmaSenha) {
					alert("Senhas não conferem");
				} else if (antigaSenha.length < 1 || novaSenha.length < 1) {
					alert("Preencha todos os campos");
				} else {
					Ajax.onreadystatechange = respostaAlterarUser;
					Ajax.open('GET', '_assets/ajax/alterarDadoUser.php?dado=senha&valor='+novaSenha+'&senha='+antigaSenha);
					Ajax.send(null);
					Ajax.close;	
				}
			break;
			case 2:
				reComEspaco = /[,{}ªº;:?/°><~^+=§*&¨%$#!´`\\\[\]|]/;
				reCPF = /([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/;
				reNumeros = /[0-9]/;
				reData = /^[1-3]{1}[0-9]{1}\/[0-1]{1}[0-9]{1}\/[1-2]{1}[09]{1}[0-9]{2}/;
				reTel = /^\([0-9]{2}\) [0-9]{4}\-[0-9]{4}/;
				reCel = /^\([0-9]{2}\) [0-9]{5}\-[0-9]{4}/;

				CPFValido = false;
				dataValida = false;
				telValido = false;
				celValido = false;
				nomeValido = false;

				senha = $('#senhaModal').val();
				nome = $("#nome").val();

				if ($("#sexoM").is(':checked')) {
					sexo = 'M';
				} else {
					sexo = 'F';
				}

				cpf = $("#cpf").val();
				nascimento = $("#dataNascimento").val();
				tel = $("#telContato").val();
				cel = $("#telCelular").val();

				if (reCPF.test(cpf)) {
					CPFValido = true;
					if (reData.test(nascimento)) {
						dataValida = true;
						if (reTel.test(tel)) {
							telValido = true;
							if (reCel.test(cel) || cel.length == 0) {
								celValido = true;
								if (/\w[ ]\w/ig.test(nome) && !(reComEspaco.test(nome)) && !reNumeros.test(nome)) {
									nomeValido = true;
								} else {
									nomeValido = false;
								}
							} else {
								celValido = false;
							}
						} else {
							telValido = false;
						}
					} else {
						dataValida = false;
					}
				} else {
					CPFValido = false;
				}

				if (senha.length < 1) {
					alert("Preencha a senha");
				} else if (CPFValido == false) {
					alert("CPF inválido");
				} else if (dataValida == false) {
					alert("Data inválida");
				} else if (telValido == false) {
					alert("Telefone inválido");
				} else if (celValido == false) {
					alert("Celular inválido");
				} else if (nomeValido == false) {
					alert("Nome inválido");
				} else {
					Ajax.onreadystatechange = respostaAlterarUser;
					Ajax.open('GET', '_assets/ajax/alterarDadoUser.php?nome='+nome+'&sexo='+sexo+'&cpf='+cpf+'&nascimento='+nascimento+'&tel='+tel+'&cel='+cel+'&senha='+senha);
					Ajax.send(null);
					Ajax.close;	
				}				
			break;
			case 3:
				reCep = /^[0-9]{5}\-[0-9]{3}$/;		
				reNumeros = /\D/;		
				CEPValido = false;
				nCasaValido = false;

				senha = $('#senhaModal').val();
				cep = $("#cep").val();
				rua = $("#rua").val();
				n = $("#nCasa").val();
				complemento = $("#complemento").val();
				bairro = $("#bairro").val();
				cidade = $("#cidade").val();
				uf = $("#uf").val();

				if (reCep.test(cep)) {
					CEPValido = true;
					if (reNumeros.test(n) || n.length < 1) {
						nCasaValido = false;
					} else {
						nCasaValido = true;
					}
				} else {
					CEPValido = false;
				}

				if (senha.length < 1) {
					alert("Preencha a senha");
				} else if (CEPValido == false) {
					alert("CEP inválido");
				} else if (nCasaValido == false) {
					alert("Número da casa inválido");
				} else {
					Ajax.onreadystatechange = respostaAlterarUser;
					Ajax.open('GET', '_assets/ajax/alterarDadoUser.php?cep='+cep+'&rua='+rua+'&n='+n+'&complemento='+complemento+'&bairro='+bairro+'&cidade='+cidade+'&uf='+uf+'&senha='+senha);
					Ajax.send(null);
					Ajax.close;	
				}				
				

			break;
		}
	}
}

function respostaAlterarUser() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			status = Ajax.responseText;
			alert(status);
		}
	}
}

function adicionarCarrinho(id) {
	id = $(id).val();
	
	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		Ajax.onreadystatechange = respostaCarrinho;
		Ajax.open('GET', '_assets/ajax/adicionarCarrinho.php?id='+id);
		Ajax.send(null);
		Ajax.close;
	}
}

function respostaCarrinho() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			$("#itens").text(Ajax.responseText);
			if (Ajax.responseText == '0') {
				alert('Você não tem produtos no carrinho');
				window.location.href='index.php'
			}
		}
	}
}

function validarCupom() {
	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		valor = $("#cupomPedido").val();

		if (valor.length < 8) {
			alert("Cupom inválido");
		} else {
			Ajax.onreadystatechange = respostaCupom;
			Ajax.open('GET', '_assets/ajax/validarCupom.php?cod='+valor);
			Ajax.send(null);
			Ajax.close;
		}
	}
}

function respostaCupom() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			cupom = Ajax.responseText;
			
			if (cupom.match(/[0-9]/)) {
				tipo = cupom.replace(/[^f^p]/ig, '');
				valor = cupom.replace(/[^0-9]/, '');
				
				if (tipo == 'F') {
					valor = "R$ " + valor;
					valor = valor.substring(0, valor.length - 2);
					valor = valorExibir(valor);
					$("#valorDesconto").text(valor);
				} else if (tipo == 'P') {
					subtotal = parseFloat($("#subtotal").text().replace(/R\$ /ig, '').replace(/[\,]/g, '.'));
					valor = parseFloat((valor * subtotal)/100);
					valor = valorExibir(valor.toString());
					valor = "R$ " + valor;
					$("#valorDesconto").text(valor);
				}
			} else {
				alert(cupom);
				$("#valorDesconto").text("R$ 0,00");
			}
			setarTotal();
		}
	}
}

function calcularFrete() {
	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		cep = $("#cep").val().replace(/[\-]/, '');
		quantidade = 0;

		for (var i = $(".quantidadeProduto").length - 1; i >= 0; i--) {
			quantidade = quantidade + (parseInt($(".quantidadeProduto").eq(i).val(), 10));
		}

		Ajax.onreadystatechange = respostaCEP;
		Ajax.open('GET', '_assets/ajax/calcularFrete.php?cep='+cep+'&quantidade='+quantidade);
		Ajax.send(null);
		Ajax.close;
	}
}

function respostaCEP() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			resposta = Ajax.responseText;

			if (/[destino]/ig.test(resposta)) {
				$("#valorFrete").text("R$ 0,00");
				alert(resposta);
			} else {
				frete = resposta.substring(0, resposta.indexOf(',') + 3);
				frete = "R$ " + frete;

				frete = valorExibir(frete);
				$("#valorFrete").text(frete);				
			}

			if ((/^[0-9]{1}/.test($("#valorFrete").text()))) {
				alert("Erro ao calcular frete para o endereço");
				$("#valorFrete").text("R$ 0,00");
			}

			setarTotal();			
		}
	}
}

function removerProduto(id) {
	$("#produto-"+id).remove();

	setarSubtotal();

	if ($("#cep").val().length >= 8) {
		calcularFrete();
	}

	if ($("#cupomPedido").val().length == 8) {
		validarCupom();
	}	

	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		Ajax.onreadystatechange = respostaCarrinho;
		Ajax.open('GET', '_assets/ajax/deletarCarrinho.php?id='+id);
		Ajax.send(null);
		Ajax.close;
	}
}

function finalizarCompra() {
	AjaxRequest();

	if (!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		cupom = $("#cupomPedido").val();

		var quantidades = $(".quantidadeProduto").serialize();

		Ajax.onreadystatechange = finalizar;
		Ajax.open('GET', '_assets/ajax/finalizar.php?'+quantidades+"&cupom="+cupom);
		Ajax.send(null);
		Ajax.close;
	}
}

function finalizar() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			alert(Ajax.responseText);
			location.reload();
		}
	}
}

function traduzir(idioma) {

	AjaxRequest();

	if(!Ajax) {
		alert("Erro na chamada Ajax");
	} else {
		Ajax.onreadystatechange = recarregar;
		Ajax.open('GET', '_assets/ajax/traduzir.php?idioma='+idioma);
		Ajax.send(null);
		Ajax.close;
	}
}

function recarregar() {
	if (Ajax.readyState == 4) {
		if (Ajax.status == 200) {
			location.reload();
		}
	}
}