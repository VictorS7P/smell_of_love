$(function(){
	var larguraTela = $(window).width();

	$(window).resize(function(){
		var novaLargura = $(window).width();
		if (novaLargura == larguraTela) {}
		else {
			location.reload();
		}
		larguraTela = novaLargura;
	});

	$('.barra-topo').css('margin-top', parseInt(($('.barra-topo').innerHeight() * -1)));

	$('#mostrar').on('click tap', function(e){
		e.preventDefault();
		
		if ($('.barra-topo').hasClass('dropped')) {
			$('.barra-topo').css('margin-top', parseInt(($('.barra-topo').innerHeight() * -1)));
		} else {
			$('.barra-topo').css('margin-top', '0px');
		}

		$('.barra-topo').toggleClass('nodropped');
		$('.barra-topo').toggleClass('dropped');
		$(this).toggleClass('rotated');
	})

	$("button[value='login']").on({
	    mouseenter: function(){
	        $("button[value='login'] i").removeClass('fa-lock  fa-unlock').addClass('fa-unlock-alt');
	    }, 
	    click: function(){
	    	$("button[value='login'] i").removeClass('fa-unlock-alt fa-lock').addClass('fa-unlock');
	    },
	    tap: function(){
	    	$("button[value='login'] i").removeClass('fa-unlock-alt fa-lock').addClass('fa-unlock');
	    }
	});

	function posicaoBtnSlide() {
		for (var i = 0; i < $('.img-slide').length; i++) {
			$('.btn-posicao-slide').eq(i).removeClass('ativo');
			if ($(".img-slide").eq(i).hasClass("ativa")) {
				var posicao =  i;
			}
		}
		$('.btn-posicao-slide').eq(posicao).addClass('ativo');
	}

	$('#slide .img-slide:eq(0)').addClass('ativa');
	posicaoBtnSlide();

	$('.btn-posicao-slide').on('click tap', function(e) {
		e.preventDefault();
		posicao = $(this).index();
		$(".ativa").removeClass('ativa');
		$('.img-slide').eq(posicao).addClass('ativa');
		posicaoBtnSlide();
	});

	function proximoSlide() {
		if ($(".ativa").next().hasClass('img-slide')) {
			$(".ativa").removeClass('ativa').next().addClass('ativa');
		} else {
			$('.ativa').removeClass('ativa');
			$('.img-slide').first().addClass('ativa');
		}
		posicaoBtnSlide();
	}

	setInterval(proximoSlide,7000);

	$('#slide-right').on('click tap', function(e) {
		proximoSlide();
	});

	$('#slide-left').on('click tap', function(e) {
		if ($(".ativa").prev().hasClass('img-slide')) {
			$(".ativa").removeClass('ativa').prev().addClass('ativa');
		} else {
			$('.ativa').removeClass('ativa');
			$('.img-slide').last().addClass('ativa');
		}
		posicaoBtnSlide();
	});

	var largura = $(window).innerWidth();
	var alturaBotao = $('.posicao button').outerWidth();
	var leftBotao = largura/2 - $(".posicao").innerWidth()/2;
	$(".posicao").css({
		'height': alturaBotao,
		'left': leftBotao
	});

	$(".img-produto").css({
		'width': $(".produto-promocao").width(),
		'height': $(".produto-promocao").width()/1.5
	});

	$('#login').on('shown.bs.modal', function () {
  		$('#emailLogin').focus()
	});


	alturaLinhaTabela = $('.linha-tabela').eq(1).innerHeight();
	alturaItemTabela = new Array;
	marginItemTabela = new Array;

	for (var i = $(".item-tabela").length - 1; i >= 0; i--) {
		alturaItemTabela[i] = $(".item-tabela").eq(i).innerHeight();
		marginItemTabela[i] = (alturaLinhaTabela / 2) - (alturaItemTabela[i] / 2);
		$(".item-tabela").eq(i).css('marginTop', marginItemTabela[i]);
	}

	$("#cpf").mask('000.000.000-00');
	$('#cep').mask('00000-000');
	$("input[type='tel']").mask('(00) 00000-0000');
	$("#precoProduto").mask('000.000.000.000.000,00', {reverse: true});
	$("#novoPrecoProduto").mask('000.000.000.000.000,00', {reverse: true});
	$("#dataNascimento").mask('00/00/0000');
	$("#telContato").mask("(00) 0000-0000");
	$("#telCelular").mask("(00) 00000-0000");
	$("#novoCupomCodigo").mask("00000000");
	$("#cupomPedido").mask("00000000");
	$("#novoCupomFixo").mask('000.000.000.000.000,00', {reverse: true});
	$("#novoPreco").mask('000.000.000.000.000,00', {reverse: true});

	var alturaTituloFooter = $("#redes").innerHeight();
	$("#fale").height(alturaTituloFooter);
	$("#formas").height(alturaTituloFooter);

	$("#fale").css('line-height', alturaTituloFooter+"px");

	if ($("#barraHeader").innerHeight() > 35) {
		$("#idiomas").removeClass("col-md-2 flex-md-last");
		$("#barraLogin").removeClass("col-md-3 offset-md-2 mb-md-0");
		$("#barraCarrinho").removeClass("col-md-4 offset-md-1 pr-md-0 mr-md-0 pl-md-0 pt-md-0");
		$("#linhaCarrinho").removeClass("justify-content-md-center");
		$("#colCarrinho").removeClass("col-md-5");
		$("#colCarrinho2").removeClass("col-md-5");
		$("#valorCarrinho").removeClass("float-md-right");
	}

	estrelaClicada = false;
	avaliacaoInicial = new Array(4);
	for (i = 0; i < $('.estrela').length; i++) {
		if ($('.estrela').eq(i).hasClass('fa-star')) {
			avaliacaoInicial[i] = 'fa-star';
		} else {
			avaliacaoInicial[i] = 'fa-star-o';
		}
	}

	$(".estrela").hover(function() {
		var estrela = $(this).index();
		do {
			$(".estrela").eq(estrela).removeClass('fa-star-o');
			$(".estrela").eq(estrela).addClass('fa-star');
			estrela--;
		} while (estrela >= 0);
		estrelaClicada = false;
	}, function() {
		if (estrelaClicada == false) {
			for (i=0; i<$(".estrela").length; i++) {
				$(".estrela").eq(i).removeClass('fa-star');
				$(".estrela").eq(i).removeClass('fa-star-o');
				$(".estrela").eq(i).addClass(avaliacaoInicial[i]);
			}
		}
	});

	if ($(window).height() >= $(document).height()) {
		$("#footer").addClass('fixed-bottom');
	} else {
		$("#footer").removeClass('fixed-bottom');
	}

	maiorH4 = 0;

	for (i = 0; i < $('.item-produto').length; i++) {
		if ($('.item-produto').eq(i).height() > maiorH4) {
			maiorH4 = $('.item-produto').eq(i).height();			
		}
	}
	$(".item-produto").height(maiorH4);
	
});

function carrinho(valor, id, max) {
	if (id != null || max != null) {
		var quantidade = $("#quantidade"+id).val();
	} else {
		var quantidade = $("#quantidadeItens").val();
	}
	
	if (valor == 'mais') {
		quantidade++;
	} else {
		quantidade--;
	}

	if (quantidade <= 1) {
		quantidade = 1;
	}

	if (quantidade >= max) {
		quantidade = max;
	}

	if (id != null || max != null) {
		$("#quantidade"+id).val(quantidade);

		setarTotalProduto(id);

		if ($("#cep").val().length >= 8) {
			calcularFrete();
		}

		if ($("#cupomPedido").val().length == 8) {
			validarCupom();
		}
	} else {
		$("#quantidadeItens").val(quantidade);
	}
}

function mostrarSenha(nome){
	$('#'+nome).attr('type', 'text');
}

function esconderSenha(){
	$("#senha").attr('type', 'password');
	$("#confirmaSenha").attr('type', 'password');
	$("#antigaSenha").attr('type', 'password');
	$("#antigaSenhaModal").attr('type', 'password');
	$("#senhaModal").attr('type', 'password');
}

function verificarSenha(item) {
	senha = $(item).val();
	segurancaSenha(0);
	if (senha.length >= 1) {
		segurancaSenha(1);
		if (senha.length >= 5 && senha.match(/[a-z]+[0-9]/)) {
			segurancaSenha(2);
			if (senha.length >= 8 && senha.match(/[<>!@#$%¨&*()]/)) {
				segurancaSenha(3);
			}
		}
	}
	validar(item);
}

function segurancaSenha(forca) {
	switch (forca) {
		case 0:
			$("#barraSenha").removeClass('w-25 w-50 w-100');
		break;
	 	case 1:
	 		$("#barraSenha").removeClass('w-50 w-100 bg-warning bg-success')
			$("#barraSenha").addClass('w-25 bg-danger');
	 	break;

	 	case 2:
	 		$("#barraSenha").removeClass('w-25 w-100 bg-danger bg-seccess');
	 		$("#barraSenha").addClass('w-50 bg-warning');
	 	break;

	 	case 3:
	 		$("#barraSenha").removeClass('w-25 w-50 bg-danger bg-warning')
	 		$("#barraSenha").addClass('w-100 bg-success');
	 	break;
	 }
}

function avaliarProduto(nota) {
	$(".estrela").removeClass('fa-star').addClass('fa-star-o');
	for (i=0; i<nota; i++) {
		$(".estrela").eq(i).removeClass('fa-star-o').addClass('fa-star');
	}
	if (nota == 1) {
		nota = nota + " estrela";
	} else {
		nota = nota + " estrelas";
	}
	$("#quantidadeEstrelas").html(nota);
	estrelaClicada = true;
}

var emailValido = false;
var senhaValida = false;
var nomeValido = false;
var sexoValido = false;
var cpfValido = false;
var dataValida = false;
var telValido = false;
var cepValido = false;
var numeroValido = false;
var cupomValido = false;

function validar(item) {
	reSemEspaco = /[ ,{}ªº;:?/°><~^+=§*&¨%$#!´`\\\[\]|]/;
	reComEspaco = /[,{}ªº;:?/°><~^+=§*&¨%$#!´`\\\[\]|]/;
	reCPF = /([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/;
	reNumeros = /[0-9]/;
	reData = /^[1-3]{1}[0-9]{1}\/[0-1]{1}[0-9]{1}\/[1-2]{1}[09]{1}[0-9]{2}/;
	reTel = /^\([0-9]{2}\) [0-9]{4}\-[0-9]{4}/;
	reCel = /^\([0-9]{2}\) [0-9]{5}\-[0-9]{4}/;
	reCep = /^[0-9]{5}\-[0-9]{3}$/;

	tipo = item.type;
	nome = item.name;
	valor = item.value;
	id = item.id;

	if (tipo == 'email') {		
	    var atpos = valor.indexOf("@");
	    var dotpos = valor.lastIndexOf(".");
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=valor.length || reSemEspaco.test(valor)) {
	    	validaErrado(id);
	        emailValido = false;
	    } else {
	    	validaCerto(id);
	        emailValido = true;
	        buscarBanco(item);
	    }
	}

	if (tipo == 'password') {
		if (!(/antiga/ig.test(nome))) {
			if (/confirma/ig.test(nome)) {
				novoNome = (nome.replace(/confirma/, '')).toLowerCase();
			} else {
				nome = nome.toLowerCase().replace(/\b[a-z]/g, function(letter) {
				    return letter.toUpperCase();
				});
				novoNome = 'confirma'+nome;
			}

			if (valor.match($("#"+novoNome).val()) && ($("#"+novoNome).val().match(valor)) && valor.length > 0) {
				validaCerto(id);
				senhaValida = true;
			} else {
				validaErrado(id);
				senhaValida = false;
			}
		}
	}


	if (tipo == 'text') {
		if (/nome/ig.test(nome)) {
			if (/\w[ ]\w/ig.test(valor) && !(reComEspaco.test(valor)) && !reNumeros.test(valor)) {
				validaCerto(id);
				nomeValido = true;
			} else {
				validaErrado(id);
				nomeValido = false;
			}
		}
	}

	if (tipo == 'radio') {
		if (/\w/ig.test(valor)) {
			sexoValido = true;
		}
	}

	if (/cpf/ig.test(nome)) {
		if (reCPF.test(valor)) {
			validaCerto(id);
			cpfValido = true;
			buscarBanco(item);
		} else {
			validaErrado(id)
			cpfValido = false;
		}
	}

	if (/data/ig.test(nome)) {
		if (reData.test(valor)) {
			dataValida = true;
			validaCerto(id);
		} else {
			dataValida = false;
			validaErrado(id);
		}
	}

	if (/tel/ig.test(nome)) {
		if (!(/cel/ig.test(nome))) {
			if (reTel.test(valor)) {
				telValido = true;
				validaCerto(id);
			} else {
				telValido = false;
				validaErrado(id);
			}
		} else {
			if (reCel.test(valor) || valor.length == 0) {
				validaCerto(id);
			} else {
				validaErrado(id);
			}
		}
	}

	if (/^cep/ig.test(nome)) {
		if (reCep.test(valor)) {
			validaCerto(id);
			cepValido = true;
		} else {
			validaErrado(id);
			cepValido = false;
		}
	}

	if (/nCasa/ig.test(nome)) {
		if (!(/\D/.test(valor))) {
			validaCerto(id);
			numeroValido = true;
		} else {
			validaErrado(id);
			numeroValido = false;
		}
	}

	if (/complemento/ig.test(nome)) {
		if (reComEspaco.test(valor)) {
			validaErrado(id);
		} else {
			validaCerto(id);
		}
	}


	if (emailValido == true && senhaValida == true && nomeValido == true && sexoValido == true && cpfValido == true && dataValida == true && telValido == true && cepValido == true && numeroValido == true) {
		$("#cadastrar").removeAttr('disabled');
		$("#alterar").removeAttr('disabled');
	} else {
		$("#cadastrar").attr('disabled', 'disabled');
		$("#alterar").attr('disabled', 'disabled');
	}

	if (nome == 'novoCupomCodigo') {
		if (valor.length != 8) {
			$("#"+id).val("");
			validaErrado(id);
			cupomValido = false;
		} else {
			validaCerto(id);
			cupomValido = true;
			buscarCupons(item);
		}
	}

	if (cupomValido == true) {
		$("#submitNovoCupom").removeAttr('disabled');
	} else {
		$("#submitNovoCupom").attr('disabled', 'disabled');
	}
}

function validaErrado(id) {
	var icon = "<span class='fa fa-remove'></span>";

	if (/senha/ig.test(id) && !(/antiga/ig.test(id))) {
		if (/confirma/ig.test(id)) {
			outraSenha = 'senha';
		} else {
			outraSenha = 'confirmaSenha';
		}

		$("#"+outraSenha).addClass('has-danger');
	    $("#"+outraSenha).parent().parent().addClass('has-danger');
	    $("#"+outraSenha).removeClass('has-success');
		$("#"+outraSenha).parent().parent().removeClass('has-success');
	   	$("#"+outraSenha).siblings('.validacaoInput').empty();
	    $("#"+outraSenha).siblings('.validacaoInput').append(icon);
	    $("#"+outraSenha).siblings('.validacaoInput').removeClass('px-0');
	}

	$("#"+id).addClass('has-danger');
	$("#"+id).parent().parent().addClass('has-danger');
	$("#"+id).removeClass('has-success');
	$("#"+id).parent().parent().removeClass('has-success');
	$("#"+id).siblings('.validacaoInput').empty();
	$("#"+id).siblings('.validacaoInput').append(icon);
	$("#"+id).siblings('.validacaoInput').removeClass('px-0');

	if (id == 'email' || id == 'cpf') {
    	texto = document.createTextNode(id + " inválido ou em uso");
    	$("#resposta-"+id).empty().append(texto);
    }
}

function validaCerto(id) {
	var icon = "<span class='fa fa-check'></span>";

	if (/senha/ig.test(id) && !(/antiga/ig.test(id))) {
		if (/confirma/ig.test(id)) {
			outraSenha = 'senha';
		} else {
			outraSenha = 'confirmaSenha'
		}

		$("#"+outraSenha).addClass('has-success');
	    $("#"+outraSenha).parent().parent().addClass('has-success');
	    $("#"+outraSenha).removeClass('has-danger');
		$("#"+outraSenha).parent().parent().removeClass('has-danger');
	   	$("#"+outraSenha).siblings('.validacaoInput').empty();
	    $("#"+outraSenha).siblings('.validacaoInput').append(icon);
	    $("#"+outraSenha).siblings('.validacaoInput').removeClass('px-0');
	}

	$("#"+id).addClass('has-success');
    $("#"+id).parent().parent().addClass('has-success');
    $("#"+id).removeClass('has-danger');
    $("#"+id).parent().parent().removeClass('has-danger');
   	$("#"+id).siblings('.validacaoInput').empty();
    $("#"+id).siblings('.validacaoInput').append(icon);
    $("#"+id).siblings('.validacaoInput').removeClass('px-0');

    if (id == 'email' || id == 'cpf') {
    	texto = document.createTextNode(id + " válido");
    	$("#resposta-"+id).empty().append(texto);
    }
}

function bloquearInputs() {
	$('input').attr('readonly', 'readonly');
}

function bloquearValorOposto(radio) {
	var nome = $(radio).attr('id');

	if (nome == 'valorFixo') {
		var outroInput = $("#novoCupomPercentual");
		var input = $("#novoCupomFixo");

	} else {
		var outroInput = $("#novoCupomFixo");
		var input = $("#novoCupomPercentual");
	}

	$(outroInput).attr('readonly', 'readonly').removeAttr('required').val("");

	$(input).attr('required', 'required').removeAttr('readonly');
}

function trocarInformacao(botao) {
	tipo = $(botao).parent().siblings('.tipoInformacao').text();
	dado = $(botao).parent().siblings('.dadoInformacao').text();

	$("#tipoInformacaoModal").val(tipo);
	$("#dadoInformacaoModal").val(dado);
}

function modalProduto(id) {
	modal = $("#carModalProduto");

	img = $(modal).children('img');
	$(img).attr('src', $(img).attr('src')+''+id);

	produto = $("#"+id);
	header = $(produto).children('.card-header');
	title = $(header).children('.card-title');
	nome = $(title).children('.nome').text();

	tipo = $(header).children('.tipo').text();

	if (/masculino/ig.test(tipo)) {
		$("#novoTipoM").attr('checked', 'checked');
	} else {
		$("#novoTipoF").attr('checked', 'checked');
	}

	conteudo = $(header).children('.conteudo').text().replace(/\D/ig, "");
	estoque = $(header).children('.estoque').text().replace(/\D/ig, "");

	corpo = $(produto).children('.card-block');
	descricaoPt = $(corpo).children('.descricaoPt').text();
	descricaoEn = $(corpo).children('.descricaoEn').text();
	descricaoDn = $(corpo).children('.descricaoDn').text();

	footer = $(produto).children('.card-footer');
	preco = $(footer).children('.card-text').children('.preco').text().replace(/[R$ ]/ig, "");

	$("#novoNomeProduto").val(nome);
	$("#novoConteudoProduto").val(conteudo);
	$("#novoEstoqueProduto").val(estoque);
	$("#novaDescricaoProdutoPt").val(descricaoPt);
	$("#novaDescricaoProdutoEn").val(descricaoEn);
	$("#novaDescricaoProdutoDn").val(descricaoDn);
	$("#novoPrecoProduto").val(preco);

	$("#excluirProduto").attr('href', 'excluirProduto.php?id='+id);
	$("#formularioNovoProduto").attr('action', 'alterarProdutos.php?id='+id);
}

function ordenarProdutos(valor) {
	if (valor < 0) {
		alert("Escolha uma opção para ordenar os produtos");
		$("#ordenar").val(0);
	} else {
		var querie = '';

		switch (valor) {
			case '0':
				querie = '0';
			break;			
			case '1':
				var querie = 'estoque';
			break;
			case '2':
				querie = 'quantidadeVendas';
			break;
			case '3':
				querie = 'avaliacao';
			break;
			case '4':
				querie = 'preco';
			break;			
		}

		if (querie != '0') {
			if (/\?tipo\=/.test(window.location.href)) {					
				link = window.location.href;
				if(/\&querie/.test(link)) {							
					pos = link.indexOf('&querie=');
					novoLink = link.substring(0, pos);
					link = novoLink;
				}
				link = link+'&querie='+querie;
			} else {
				link = window.location.href;
				pos = link.indexOf(/\?/);
				novoLink = link.substring(0, pos);
				link = novoLink;
				link = link+'?querie='+querie;
			}
		} else {
			link = window.location.href;

			if(/\&querie/.test(link)) {							
				pos = link.indexOf('&querie=');
				novoLink = link.substring(0, pos);
				link = novoLink;
			}

			if(/\?querie/.test(link)) {							
				pos = link.indexOf('?querie=');
				novoLink = link.substring(0, pos);
				link = novoLink;
			}
		}
		window.location.href = link;
	}
}

function rodar(botao) {
	link = window.location.href;

	if(/\&modo\=/.test(link)) {							
		pos = link.indexOf('&modo=');
		novoLink = link.substring(0, pos);
		link = novoLink;
	} else if (/\?modo\=/.test(link)) {							
		pos = link.indexOf('?modo=');
		novoLink = link.substring(0, pos);
		link = novoLink;
	}

	if (/\?tipo\=/.test(link) || /\?querie\=/.test(link)) {
		link = link + '&modo=desc';
	} else {
		link = link + '?modo=desc';
	}

	window.location.href = link;
	$(botao).addClass('rotated');
}

function desrodar(botao) {
	link = window.location.href;

	if(/\&modo\=/.test(link)) {							
		pos = link.indexOf('&modo=');
		novoLink = link.substring(0, pos);
		link = novoLink;
	} else if (/\?modo\=/.test(link)) {							
		pos = link.indexOf('?modo=');
		novoLink = link.substring(0, pos);
		link = novoLink;
	}
	window.location.href = link;
}

function acessarProduto(id) {
	window.location.href = "produto.php?produto="+id;
}

function modalPromocao(botao) {
	id = $(botao).val();
	$("#botaoPromo").val(id);	
}

function pesquisar() {
	if ($("#pesquisa").val().length <= 2) {
		alert("Digite no mínimo 3 caracteres para pesquisar");
	}  else {
		window.location.href = "pesquisa.php?pesquisa="+$("#pesquisa").val();
	}
}

function valorModalPerfil(valor) {
	$('#modal-senha-antiga').modal({
		show: true
	});
	$("#alterarDadosBtn").attr('onclick', 'alterarDadoUser('+valor+')');

}

function setarTotalProduto(id) {
	quantidade = $('#quantidade'+id).val();
	valor = $("#preco-"+id).text();
	preco = parseFloat(valor.replace(/R\$ /ig, '').replace(/[\,]/g, '.'));
	total = "R$ " + (preco * quantidade);

	total = valorExibir(total);

	$("#valor-total-"+id).text(total);
	setarSubtotal();
}

function setarSubtotal() {
	subtotal = 0;
	var tam = $(".preco-total").length;

	for (var i = 0; i < tam; i++ ) {
		valor = $(".preco-total").eq(i).text();
		subtotal = subtotal + parseFloat(valor.replace(/R\$ /ig, '').replace(/[\,]/g, '.'));
	}

	subtotal = "R$ " + subtotal;

	subtotal = valorExibir(subtotal);

	$("#subtotal").text(subtotal);
	setarTotal();
}

function setarTotal() {
	subtotal = parseFloat($("#subtotal").text().replace(/[R\$ ]/g, '').replace(/\,/g, '.'));
	frete = parseFloat($("#valorFrete").text().replace(/[R\$ ]/g, '').replace(/\,/g, '.'));
	desconto = parseFloat($("#valorDesconto").text().replace(/[^0-9][\$]/, '').replace(/\,/g, '.'));

	total = subtotal - desconto + frete;
	total = "R$ " + total;

	total = valorExibir(total);

	$("#totalPedido").text(total);
}

function valorExibir(valor) {
	if (/[\.]/.test(valor)) {
		valor = valor.replace(/[\.]/, ",");
	}

	if (/[\,]/.test(valor)) {
		casaVirgula = valor.indexOf(",");

		if (valor.length - 2 == casaVirgula) {
			valor = valor + "0";
		}

		valor = valor.substring(0, casaVirgula + 3);
	} else {
		valor = valor + ",00";
	}	

	return valor;
}