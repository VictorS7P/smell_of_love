<?php 

	if (isset($_COOKIE['login'])) {
		setcookie('login', '', time()-1);
		if ($_COOKIE['login'] == 'inválido') {
			echo "	<script>
						alert('Login Inválido');
						window.location.reload()
					</script>";
		}		
	}

	include 'head.php';
?>
	<title>Smell of Love | <?php echo $lang["Home"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			setarHeader();
		?>
		<section>
			<div id="slide" class="col-12">
				<div class="row clearfix">
					<button id="slide-left" class="btn-slide btn rounded-0 btn-sm border-0"><i class="fa fa-chevron-left"></i></button>
					<button id="slide-right" class="btn-slide btn rounded-0 btn-sm border-0"><i class="fa fa-chevron-right"></i></button>
					<div class="posicao">
						<button class="btn btn-sm btn-posicao-slide rounded-circle border-0"></button>
						<button class="btn btn-sm btn-posicao-slide rounded-circle border-0"></button>
						<button class="btn btn-sm btn-posicao-slide rounded-circle border-0"></button>
					</div>
					<div class="col-12 pr-0 pl-0 img-slide">
						<a href="#">
							<div class="slide-fundo"></div>						
							<img src="<?php echo $lang["link-slide1"]; ?>" alt="imagem slide 1" class="imagem-slide">
						</a>
					</div>
					<div class="col-12 pr-0 pl-0 img-slide">
						<a href="#">
							<div class="slide-fundo"></div>
							<img src="_assets/imagens/imagem-slide-2.png" alt="imagem slide 2" class="imagem-slide">
						</a>
					</div>
					<div class="col-12 pr-0 pl-0 img-slide">
						<a href="#">
							<div class="slide-fundo"></div>
							<img src="_assets/imagens/imagem-slide-3.png" alt="imagem slide 3" class="imagem-slide">
						</a>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="col-12 promocoes pb-3">
				<div class="row clearfix">
					<div class="col-12">
						<div class="row justify-content-center">
							<h2 class="text-center pt-3 pb-2"><?php echo $lang["Promoções"]; ?></h2>
						</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-12 produtos produtos-mensal">
						<div class="row clearfix justify-content-center">
						<?php
							$sql = "SELECT * FROM produtos WHERE promocao = 'S' WHERE estoque > 0 ORDER BY id DESC LIMIT 4;";

							$res = $con->query($sql);

							if ($res) {
								while ($linha = $res->fetch_assoc()) { 
						?>
							<div class="col-10 col-sm-5 col-md-2 produto produto-promocao p-1 m-1">
								<img src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="Produto em Promoção <?php echo $linha['id']; ?>" class="p-0 m-0 img-produto img-fluid">
								<h4 class="card-title my-1 item-produto text-center" onclick="acessarProduto('<?php echo $linha['id']; ?>')"><?php echo $linha['nome']; ?></h4>
								<p class="produto-descricao text-center pt-0 mb-1 fonte-normal">
									<small class="fonte-normal"><?php echo $lang["Conteúdo"]; ?>: <?php echo $linha['conteudo']; ?>ml</small><br>
									<?php echo $linha[$lang['descricao']]; ?>
								</p>
								<p class="produto-preco text-center mb-1">R$<?php echo $linha['preco']; ?></p>
								<button value="<?php echo $linha['id']; ?>" class="btn btn-block btn-success btn-sm rounded-0 btn-adc-car d-block mx-auto" onclick="adicionarCarrinho(this)">
									<p class="p-0 m-0">Adicionar ao carrinho</p>
								</button>
							</div>
						<?php 	}
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="col-12 femininos pb-3">
				<div class="row clearfix">
					<div class="col-12">
					<hr>
					<div class="row justify-content-center">
						<h2 class="pb-2 text-center"><?php echo $lang["Últimos Produtos Femininos"]; ?>
							<a class="btn btn-sm rounded-0 ver-todos mb-2" href="feminino.php">
								<p class="p-2 m-0"><?php echo $lang["Ver todos"]; ?></p>
							</a>
						</h2>
					</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-12 produtos produtos-femininos">
						<div class="row clearfix justify-content-center">
							<?php
							$sql = "SELECT * FROM produtos WHERE tipo = 'F' AND estoque > 0 ORDER BY id DESC LIMIT 4;";

							$res = $con->query($sql);

							if ($res) {
								while ($linha = $res->fetch_assoc()) { 
						?>
							<div class="col-10 col-sm-5 col-md-2 produto produto-feminino p-1 m-1">
								<img src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="Produto Feminino <?php echo $linha['id']; ?>" class="p-0 m-0 img-produto img-fluid">
								<h4 class="card-title my-1 item-produto text-center" onclick="acessarProduto('<?php echo $linha['id']; ?>')"><?php echo $linha['nome']; ?></h4>
								<p class="produto-descricao text-center pt-0 mb-1 fonte-normal">
									<small class="fonte-normal"><?php echo $lang["Conteúdo"]; ?>: <?php echo $linha['conteudo']; ?>ml</small><br>
									<?php echo $linha[$lang['descricao']]; ?>
								</p>
								<p class="produto-preco text-center mb-1">R$<?php echo $linha['preco']; ?></p>
								<button value="<?php echo $linha['id']; ?>" class="btn btn-block btn-success btn-sm rounded-0 btn-adc-car d-block mx-auto" onclick="adicionarCarrinho(this)">
									<p class="p-0 m-0"><?php echo $lang["Adicionar ao carrinho"]; ?></p>
								</button>
							</div>
						<?php 	}
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section>
			<div class="col-12 masculinos pb-3">
				<div class="row clearfix">
					<div class="col-12">
					<hr>
					<div class="row justify-content-center">
						<h2 class="pb-2 text-center"><?php echo $lang["Últimos Produtos Masculinos"]; ?>
							<a class="btn btn-sm rounded-0 ver-todos mb-2" href="masculino.php">
								<p class="p-2 m-0"><?php echo $lang["Ver todos"]; ?></p>
							</a>
						</h2>
					</div>
					</div>
				</div>
				<div class="row clearfix">
					<div class="col-12 produtos produtos-masculinos">
						<div class="row clearfix justify-content-center">
							<?php
							$sql = "SELECT * FROM produtos WHERE tipo = 'M' AND estoque > 0 ORDER BY id DESC LIMIT 4;";

							$res = $con->query($sql);

							if ($res) {
								while ($linha = $res->fetch_assoc()) { 
						?>
							<div class="col-10 col-sm-5 col-md-2 produto produto-masculino p-1 m-1">
								<img src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="Produto Masculino <?php echo $linha['id']; ?>" class="p-0 m-0 img-produto img-fluid">
								<h4 class="card-title my-1 item-produto text-center" onclick="acessarProduto('<?php echo $linha['id']; ?>')"><?php echo $linha['nome']; ?></h4>
								<p class="produto-descricao text-center pt-0 mb-1 fonte-normal">
									<small class="fonte-normal"><?php echo $lang["Conteúdo"]; ?>: <?php echo $linha['conteudo']; ?>ml</small><br>
									<?php echo $linha[$lang['descricao']]; ?>
								</p>
								<p class="produto-preco text-center mb-1">R$<?php echo $linha['preco']; ?></p>
								<button value="<?php echo $linha['id']; ?>" class="btn btn-block btn-success btn-sm rounded-0 btn-adc-car d-block mx-auto" onclick="adicionarCarrinho(this)">
									<p class="p-0 m-0"><?php echo $lang["Adicionar ao carrinho"]; ?></p>
								</button>
							</div>
						<?php 	}
							}
						?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<?php include 'footer.php'; ?>
</html>
					