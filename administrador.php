<?php 
	include 'acessoRestrito.php';
	include 'head.php';
?>
	<title>Smell of Love |<?php echo $lang["Painel"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			include 'headerAdm.php';

			$sql = "SELECT * FROM mensagens WHERE checada = 'N'";
			$res = $con->query($sql);
		?>
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row">
					<div class="col-12">
						<h2 class="display-4 text-center fonte-normal"><?php echo $lang["Painel Administrativo"]; ?></h2>
					</div>
				</div>
				<div class="row justify-content-center">
					<a href='index.php' class="col-12 col-md-5 item-administrativo btn-padrao m-md-2 my-1 fonte-normal text-center"><?php echo $lang["Acessar Site"]; ?></a>
					<a href="admProdutos.php" class="col-12 col-md-5 item-administrativo btn-padrao m-md-2 my-1 fonte-normal text-center"><?php echo $lang["Alterar/Cadastrar Produtos"]; ?></a>
					<a href="admSobre.php" class="col-12 col-md-5 item-administrativo btn-padrao m-md-2 my-1 fonte-normal text-center"><?php echo $lang["Alterar Sobre"]; ?></a>
					<a href="admCupons.php" class="col-12 col-md-5 item-administrativo btn-padrao m-md-2 my-1 fonte-normal text-center"><?php echo $lang["Alterar Cupons de Desconto"]; ?></a>
					<a href="admInfos.php" class="col-12 col-md-5 item-administrativo btn-padrao m-md-2 my-1 fonte-normal text-center"><?php echo $lang["Alterar Informações Gerais"]; ?></a>
					<a href="admMensagens.php" class="col-12 col-md-5 item-administrativo btn-padrao m-md-2 my-1 fonte-normal text-center"><?php echo "$res->num_rows {$lang['novas mensagens']}"; ?></a>
					<a href="admPromocao.php" class="col-12 col-md-5 item-administrativo btn-padrao m-md-2 my-1 fonte-normal text-center"><?php echo $lang["Adicionar Promoção"]; ?></a>
				</div>
			</div>
		<?php
			mysqli_close($con);
			include 'footer.php';
		?>
</html>