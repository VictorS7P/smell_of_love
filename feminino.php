<?php 
	include 'head.php';
?>
	<title>Smell of Love | <?php echo $lang["Femininos"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			setarHeader();			
		?>
			<section>
				<div class="col-12 col-md-8 offset-md-2">
					<div class="row">
						<div class="col-12 my-2">
							<h1 class="display-4 text-center titulo-feminino py-1"><?php echo $lang["Produtos Femininos"]; ?></h1>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
						<?php
							$sql = "SELECT * FROM produtos WHERE tipo = 'F' AND estoque > 0";

							if (isset($_GET['querie'])) {
							 	$ordem = $_GET['querie'];
							 	$sql .= " ORDER BY $ordem";

							 	if(isset($_GET['modo'])) {
							 		$sql .= " DESC";
							 	}
							}		

							$res = $con->query($sql);

							if ($res->num_rows > 0) {
						?>
							<select name="ordenar" id="ordenar" class="custom-select" onchange="ordenarProdutos(this.value)">
								<option class="fonte-normal" value="-1"><?php echo $lang["Ordenar Por"]; ?></option>
								<option class="fonte-normal" value="0"><?php echo $lang["Nada"]; ?></option>
								<option class="fonte-normal" value="1"><?php echo $lang["Estoque"]; ?></option>
								<option class="fonte-normal" value="2"><?php echo $lang["Quantidade de vendas"]; ?></option>
								<option class="fonte-normal" value="3"><?php echo $lang["Avaliação"]; ?></option>
								<option class="fonte-normal" value="4"><?php echo $lang["Preço"]; ?></option>
							</select>
							<button class="btn btn-sm btn-secondary m-2 <?php if(isset($_GET['modo'])) {echo 'rotated';} ?>" onclick="<?php if(isset($_GET['modo'])) {echo 'des';} ?>rodar(this)">
								<span class="fa fa-arrow-up"></span>
							</button>
						<?php
							} else {
								echo "
										<script>
											alert('{$lang["Nenhum Produto Feminino Cadastrado"]}');
											window.location.href = 'index.php';
										</script>";
							}
						?>									
							<div class="row">
							<?php 
								if ($res) {
									while ($linha = $res->fetch_assoc()) { 
							?>
								<div class="col-12 col-sm-6 px-0">
									<div class="card m-2 rounded-0 card-masculino">
								 		<img class="card-img-top w-100 img-fluid rounded-0" src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="Card image cap">
										<div class="card-header border-0">
								    		<h4 class="card-title my-1 pb-0 mb-0 item-produto" onclick="acessarProduto('<?php echo $linha['id']; ?>')"><?php echo $linha['nome']; ?></h4>
								    		<br>
											<small class="fonte-normal text-center"><?php echo $lang["Conteúdo"]; ?>: <?php echo $linha['conteudo']; ?>ml</small>
								    	</div>
										<div class="card-block">						    	
								    		<p class="card-text lead fonte-normal text-justify" style="text-indent: 0">
								    			<?php echo $linha[$lang["descricao"]]; ?>
								    		</p>
								   		</div>
								   		<div class="card-footer border-0">
								   			<p class="card-text lead fonte-normal">
								   				R$<?php echo $linha['preco']; ?> 
								   				<button value="<?php echo $linha['id']; ?>" class="btn btn-padrao float-right rounded-0" onclick="adicionarCarrinho(this)"><?php echo $lang["Adicionar ao carrinho"]; ?></button>
								   			</p>
								   		</div>
								  	</div>
						  		</div>
						  <?php 
						  			} 
						  		}
						  	?>
							</div>			
						</div>
					</div>
				</div>
			</section>
		<?php include 'footer.php'; ?>
</html>