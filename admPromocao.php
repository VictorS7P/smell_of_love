<?php 
	include 'head.php';
	include 'acessoRestrito.php'
?>
	<title>Smell of Love | <?php echo $lang["Promoções"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php include 'headerAdm.php'; ?>
		<section>
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row mx-2">
					<div class="col-12 my-3">
						<h2 class="text-center display-4">
							<?php echo $lang["Produtos em Promoção"]; ?>
						</h2>
						<p class="m-0 text-center"><small class="text-center fonte-normal"><?php echo $lang["Sempre que um produto tiver um preço menor definido ele será considerado em promoção"]; ?></small></p>
						<div class="row">
						<?php 
							$sql = "SELECT * FROM produtos WHERE promocao = 'S';";

							$res = $con->query($sql);

							if ($res) {
								while ($linha = $res->fetch_assoc()) { 
						?>
							<div class="col-12 col-sm-4">
								<div class="card m-2 rounded-0 card-masculino">
							 		<img class="card-img-top w-100 img-fluid rounded-0" src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="Card image cap">
									<div class="card-header border-0">
							    		<h4 class="card-title my-1 item-produto" onclick="acessarProduto('<?php echo $linha['id']; ?>')"><?php echo $linha['nome']; ?></h4>
							    		<br>
										<small class="fonte-normal text-center"><?php echo $lang["Conteúdo"];?>: <?php echo $linha['conteudo']; ?>ml</small>
							    	</div>
									<div class="card-block">						    	
							    		<p class="card-text lead fonte-normal text-justify" style="text-indent: 0">
							    			R$<?php echo $linha['preco']; ?>
							    		</p>
							   		</div>
							   		<div class="card-footer text-center">
							   			<button class="btn btn-success" value="<?php echo $linha['id']; ?>" data-toggle='modal' data-target='#promocao' onclick='modalPromocao(this)'>
							   				<?php echo $lang["Alterar Preço"]; ?>
							   			</button>
							   		</div>						   		
							  	</div>
					  		</div>
					  	<?php 
					  			} 
					  		}
					  	?>		
					  	</div>				
					</div>
				</div>
				<div class="row mx-2">
					<div class="col-12 my-3">
						<h2 class="text-center display-4">
							<?php echo $lang["Outros Produtos"]; ?>
						</h2>						
						<div class="row">
						<?php 
							$sql = "SELECT * FROM produtos WHERE promocao = 'N';";

							$res = $con->query($sql);

							if ($res) {
								while ($linha = $res->fetch_assoc()) { 
						?>
							<div class="col-12 col-sm-4">
								<div class="card m-2 rounded-0 card-masculino">
							 		<img class="card-img-top w-100 img-fluid rounded-0" src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="Card image cap">
									<div class="card-header border-0">
							    		<h4 class="card-title my-1 item-produto" onclick="acessarProduto('<?php echo $linha['id']; ?>')"><?php echo $linha['nome']; ?></h4>
							    		<br>
										<small class="fonte-normal text-center"><?php echo $lang["Conteúdo"]; ?>: <?php echo $linha['conteudo']; ?>ml</small>
							    	</div>
									<div class="card-block">						    	
							    		<p class="card-text lead fonte-normal text-justify" style="text-indent: 0">
							    			R$<?php echo $linha['preco']; ?>
							    		</p>
							   		</div>
							   		<div class="card-footer text-center">
							   			<button class="btn btn-success" value="<?php echo $linha['id']; ?>" data-toggle='modal' data-target='#promocao' onclick='modalPromocao(this)'>
							   				<?php echo $lang["Alterar Preço"]; ?>
							   			</button>
							   		</div>						   		
							  	</div>
					  		</div>
					  	<?php 
					  			} 
					  		}
					  		mysqli_close($con);
					  	?>		
					  	</div>		
					</div>
				</div>
			</div>

			<div id="promocao" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header justify-content-center">
						<div class="modal-title titulo-modal">
							<h2 class="text-center fonte-normal display-4"><?php echo $lang["Novo Preço"]; ?></h2>
						</div>
					</div>
					<div class="modal-body">
						<div class="col-12">
							<div class="row justify-content-center">
								<input id="novoPreco" type="text" class="w-50 mx-2 form-control" required="required">
								<button id="botaoPromo" value="" class="mx-2 btn btn-success" onclick="promocao(this)">
									<?php echo $lang["Ok"]; ?>!
								</button>
							</div>
						</div>
					</div>				
				</div>
			</div>
		</div>
		</section>
		<?php include 'footer.php'; ?>
</html>