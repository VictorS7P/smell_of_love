<?php 
	// admCupons.php
	$lang["cupons de desconto"] = "Cupons de desconto";
	$lang["cadastrar novo cupom"] = "Cadastrar novo cupom";
	$lang["Código"] = "Código";
	$lang["Valor Fixo"] = "Valor Fixo";
	$lang["Valor Percentual"] = "Valor Percentual";
	$lang["Cadastrar"] = "Cadastrar";
	$lang["O código deve conter 8 números"] = "O código deve conter 8 números";
	$lang["Cupons cadastrados"] = "Cupons cadastrados";
	$lang["Descrição"] = "Descrição";
	$lang["Deletar"] = "Deletar";
	$lang["descricao"] = "descricao";

	// admInfos.php
	$lang["Informações Gerais"] = "Informações Gerais";
	$lang["Alterar informações gerais do site"] = "Alterar informações gerais do site";
	$lang["Item"] = "Item";
	$lang["Valor atual"] = "Valor atual";
	$lang["Alterar"] = "Alterar";
	$lang["e-mail"] = "e-mail";
	$lang["2º e-mail"] = "2º e-mail";
	$lang["Telefone"] = "Telefone";
	$lang["2º telefone"] = "2º telefone";
	$lang["Celular"] = "Celular";
	$lang["Link página Facebook"] = "Link página Facebook";
	$lang["Link Instagram"] = "Link Instagram";
	$lang["Snapchat"] = "Snapchat";
	$lang["Rua"] = "Rua";
	$lang["Nº"] = "Nº";
	$lang["Bairro"] = "Bairro";
	$lang["CEP"] = "CEP";
	$lang["Cidade"] = "Cidade";
	$lang["Alterar Dado"] = "Alterar Dado";
	$lang["Alterar"] = "Alterar";

	// administrador.php
	$lang["Painel"] = "Painel";
	$lang["Painel Administrativo"] = "Painel Administrativo";
	$lang["Acessar Site"] = "Acessar Site";
	$lang["Alterar/Cadastrar Produtos"] = "Alterar/Cadastrar Produtos";
	$lang["Alterar Sobre"] = "Alterar Sobre";
	$lang["Alterar Cupons de Desconto"] = "Alterar Cupons de Desconto";
	$lang["Alterar Informações Gerais"] = "Alterar Informações Gerais";
	$lang["novas mensagens"] = "novas mensagens";
	$lang["Adicionar Promoção"] = "Adicionar Promoção";

	// admMensagens.php
	$lang["Mensagens"] = "Mensagens";
	$lang["Arquivadas"] = "Arquivadas";
	$lang["Arquivar"] = "Arquivar";
	$lang["Responder"] = "Responder";
	$lang["Excluir"] = "Excluir";
	$lang["Mensagens Arquivadas"] = "Mensagens Arquivadas";
	$lang["Recarregue a página para atualizar as mensagens"] = "Recarregue a página para atualizar as mensagens";

	// admProdutos.php
	$lang["Imagem nas dimensões erradas... Cadastre somente imagens 400x200..."] = "Imagem nas dimensões erradas... Cadastre somente imagens 400x200...";
	$lang["Produtos"] = "Produtos";
	$lang["Cadastrar Produto"] = "Cadastrar Produto";
	$lang["Masculinos"] = "Masculinos";
	$lang["Produtos Cadastrados"] = "Produtos Cadastrados";
	$lang["Femininos"] = "Femininos";
	$lang["Ordenar Por"] = "Ordenar Por";
	$lang["Nada"] = "Nada";
	$lang["Estoque"] = "Estoque";
	$lang["Quantidade de vendas"] = "Quantidade de vendas";
	$lang["Avaliação"] = "Avaliação";
	$lang["Preço"] = "Preço";
	$lang["Ver Todos"] = "Ver Todos";
	$lang["Masculino"] = "Masculino";
	$lang["Feminino"] = "Feminino";
	$lang["em estoque"] = "em estoque";
	$lang["vendidos"] = "vendidos";
	$lang["Alterar/Excluir"] = "Alterar/Excluir";
	$lang["Cadastrar novo produto"] = "Cadastrar novo produto";
	$lang["Nome"] = "Nome";
	$lang["Nome do produto..."] = "Nome do produto...";
	$lang["Quantidade"] = "Quantidade";
	$lang["Imagem"] = "Imagem";
	$lang["Tamanho"] = "Tamanho";
	$lang["Tipo"] = "Tipo";
	$lang["Descrição"] = "Descrição";
	$lang["Descrição em português"] = "Descrição em português";
	$lang["Descrição em inglês"] = "Descrição em inglês";
	$lang["Descrição em dinamarquês"] = "Descrição em dinamarquês";
	$lang["Conteúdo do frasco (em ml)"] = "Conteúdo do frasco (em ml)";
	$lang["Preço (em R$)"] = "Preço (em R$)";
	$lang["Cadastrar"] = "Cadastrar";
	$lang["Alterar/Excluir produto"] = "Alterar/Excluir produto";
	$lang["Conteúdo do frasco"] = "Conteúdo do frasco";
	$lang["Estoque"] = "Estoque";
	$lang["Preço"] = "Preço";
	$lang["Alterar"] = "Alterar";
	$lang["Excluir"] = "Excluir";
	$lang["Nome"] = "Nome";
	$lang["Somente números"] = "Somente números";

	// admPromocao.php
	$lang["Promoções"] = "Promoções";
	$lang["Produtos em Promoção"] = "Produtos em Promoção";
	$lang["Sempre que um produto tiver um preço menor definido ele será considerado em promoção"] = "Sempre que um produto tiver um preço menor definido ele será considerado em promoção";
	$lang["Conteúdo"] = "Conteúdo";
	$lang["Alterar Preço"] = "Alterar Preço";
	$lang["Outros Produtos"] = "Outros Produtos";
	$lang["Novo Preço"] = "Novo Preço";
	$lang["Ok"] = "Ok";

	// admSobre.php
	$lang["Sobre"] = "Sobre";
	$lang["História"] = "História";
	$lang["novaHistoriaPt"] = "Digite uma nova história (em português), deixe em branco para não alterar os valores!";
	$lang["novaHistoriaEn"] = "Digite uma nova história (em inglês), deixe em branco para não alterar os valores!";
	$lang["novaHistoriaDn"] = "Digite uma nova história (em dinamarquês), deixe em branco para não alterar os valores!";
	$lang["Missão"] = "Missão";
	$lang["novaMissãoPt"] = "Digite uma nova missão (em português), deixe em branco para não alterar os valores!";
	$lang["novaMissãoEn"] = "Digite uma nova missão (em inglês), deixe em branco para não alterar os valores!";
	$lang["novaMissãoDn"] = "Digite uma nova missão (em dinamarquês), deixe em branco para não alterar os valores!";
	$lang["Visão"] = "Visão";
	$lang["novaVisãoPt"] = "Digite uma nova visão (em português), deixe em branco para não alterar os valores!";
	$lang["novaVisãoEn"] = "Digite uma nova visão (em inglês), deixe em branco para não alterar os valores!";
	$lang["novaVisãoDn"] = "Digite uma nova visão (em dinamarquês), deixe em branco para não alterar os valores!";
	$lang["Valores"] = "Valores";
	$lang["novosValoresPt"] = "Digite novos valores (em português), deixe em branco para não alterar os valores!";
	$lang["novosValoresEn"] = "Digite novos valores (em inglês), deixe em branco para não alterar os valores!";
	$lang["novosValoresDn"] = "Digite novos valores (em dinamarquês), deixe em branco para não alterar os valores!";
	$lang["AlterarDados"] = "AlterarDados";

	// cadastrar.php
	$lang["Cadastrar"] = "Cadastrar";
	$lang["Login e Senha"] = "Login e Senha";
	$lang["Senha"] = "Senha";
	$lang["Confirme sua senha"] = "Confirme sua senha";
	$lang["Informações Pessoais"] = "Informações Pessoais";
	$lang["Nome completo"] = "Nome completo";
	$lang["Sexo"] = "Sexo";
	$lang["CPF"] = "CPF";
	$lang["Data de nascimento"] = "Data de nascimento";
	$lang["Telefone para contato"] = "Telefone para contato";
	$lang["Telefone celular"] = "Telefone celular";
	$lang["Desejo receber ofertas da Smell por e-mail"] = "Desejo receber ofertas da Smell por e-mail";
	$lang["Desejo receber ofertas da Smell pelo celular"] = "Desejo receber ofertas da Smell pelo celular";
	$lang["Informações para Entrega"] = "Informações para Entrega";
	$lang["CEP"] = "CEP";
	$lang["Rua"] = "Rua";
	$lang["Complemento"] = "Complemento";
	$lang["Bairro"] = "Bairro";
	$lang["Cidade"] = "Cidade";

	// checkout.php
	$lang["Checkout"] = "Checkout";
	$lang["Você não tem produtos no carrinho"] = "Você não tem produtos no carrinho";
	$lang["Minha sacola de compras"] = "Minha sacola de compras";
	$lang["Produto"] = "Produto";
	$lang["Valor"] = "Valor";
	$lang["Quantidade"] = "Quantidade";
	$lang["Total"] = "Total";
	$lang["Excluir"] = "Excluir";
	$lang["Subtotal"] = "Subtotal";
	$lang["Desconto"] = "Desconto";
	$lang["Frete"] = "Frete";
	$lang["Total"] = "Total";
	$lang["OK"] = "OK";
	$lang["Calcular"] = "Calcular";
	$lang["Finalizar Compra"] = "Finalizar Compra";
	$lang["Digite seu cupom de desconto"] = "Digite seu cupom de desconto";
	$lang["Digite seu CEP"] = "Digite seu CEP";

	// contato.php
	$lang["Contato"] = "Contato";
	$lang["Fale Conosco"] = "Fale Conosco";
	$lang["Mensagem"] = "Mensagem";
	$lang["Enviar"] = "Enviar";

	// feminino.php
	$lang["Produtos Femininos"] = "Produtos Femininos";
	$lang["Adicionar ao carrinho"] = "Adicionar ao carrinho";
	$lang["Nenhum Produto Feminino Cadastrado"] = "Nenhum Produto Feminino Cadastrado";

	// footer.php
	$lang["Nossas redes sociais"] = "Nossas redes sociais";
	$lang["Fale conosco"] = "Fale conosco";
	$lang["Formas de pagamento"] = "Formas de pagamento";
	$lang["Todos os direitos reservados"] = "Todos os direitos reservados";
	$lang["nº"] = "nº";

	// head.php
	$lang["lang"] = "pt-bt";

	// header.php
	$lang["Login"] = "Login";
	$lang["Criar uma conta"] = "Criar uma conta";
	$lang["Meu carrinho"] = "Meu carrinho";
	$lang["Finalizar"] = "Finalizar";
	$lang["Pesquisar"] = "Pesquisar";
	$lang["Continuar online"] = "Continuar online";
	$lang["Logar"] = "Logar";
	$lang["E-mail"] = "E-mail";
	$lang["Senha"] = "Senha";

	// headerAdm.php
	$lang["Área do administrador"] = "Área do administrador";
	$lang["Sair"] = "Sair";

	// headerLogado.php
	$lang["Perfil"] = "Perfil";
	$lang["Administrativo"] = "Administrativo";
	$lang["Meu carrinho"] = "Meu carrinho";
	$lang["Bem-vindo"] = "Bem-vindo";

	// index.php
	$lang["link-slide1"] = "_assets/imagens/imagem-slide-1.png";
	$lang["Home"] = "Home";
	$lang["Promoções"] = "Promoções";
	$lang["Últimos Produtos Femininos"] = "Últimos Produtos Femininos";
	$lang["Ver todos"] = "Ver todos";
	$lang["Últimos Produtos Masculinos"] = "Últimos Produtos Masculinos";

	// masculino.php
	$lang["Produtos Masculinos"] = "Produtos Masculinos";
	$lang["Nenhum Produto Masculino Cadastrado"] = "Nenhum Produto Masculino Cadastrado";

	// perfil.php
	$lang["Meu Perfil"] = "Meu Perfil";
	$lang["Faça login para continuar"] = "Faça login para continuar";
	$lang["Meus Dados"] = "Meus Dados";
	$lang["Alterar senha"] = "Alterar senha";
	$lang["Alterar Dados Pessoais"] = "Alterar Dados Pessoais";
	$lang["Alterar Dados de Entrega"] = "Alterar Dados de Entrega";
	$lang["Alterar Senha"] = "Alterar Senha";
	$lang["Digite uma nova senha"] = "Digite uma nova senha";
	$lang["Confirme sua nova senha"] = "Confirme sua nova senha";
	$lang["Digite sua antiga senha"] = "Digite sua senha antiga";
	$lang["alterar"] = "alterar";
	$lang["Alterar Dados"] = "Alterar Dados";
	$lang["Digite sua senha para continuar"] = "Digite sua senha para continuar";
	$lang["alterar"] = "alterar";

	// pesquisa.php
	$lang["Pesquisa"] = "Pesquisa";
	$lang["Resultados da busca para"] = "Resultados da busca para";
	$lang["Nenhum Resultado Encontrado"] = "Nenhum Resultado Encontrado";

	// produto.php
	$lang["em estoque"] = "em estoque";
	$lang["vendidos"] = "vendidos";
	$lang["Sua avaliação atual"] = "Sua avaliação atual";
 	$lang["Não avaliado ainda"] = "Não avaliado ainda";
 	$lang["Comentários"] = "Comentários";
 	$lang["Nenhum comentário até o momento"] = "Nenhum comentário até o momento";
 	$lang["Adicionar Comentário"] = "Adicionar Comentário";
 	$lang["Comentar"] = "Comentar";
 	$lang["Dar"] = "Dar";
 	$lang["para o produto"] = "para o produto";
 	$lang["Cancelar"] = "Cancelar";
 	$lang["Avaliar"] = "Avaliar";

 	// sobre.php
	$lang["historia"] = "historiaPt";
 	$lang["missao"] = "missaoPt";
 	$lang["visao"] = "visaoPt";
 	$lang["valores"] = "valoresPt";
?>