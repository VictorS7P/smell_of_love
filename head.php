<?php
	if (isset($_COOKIE['idioma'])) {
		if ($_COOKIE['idioma'] == "en") {
			include 'en.php';
		} else if ($_COOKIE['idioma'] == "dn") {
			include 'dn.php';
		}
	} else {
		include 'pt-br.php';
	}
	include 'funcoes.php'; 
	carregarSessao();
?>

<!DOCTYPE html>
<html lang="<?php $lang["lang"]; ?>">
<head>
	<meta charset="utf-8">
	<link rel="icon" href="_assets/imagens/icone.png" type="image/gif" sizes="16x16 32x32 64x64">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=yes">
    <link rel="stylesheet" href="_assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="_assets/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="_assets/_design/css.css">