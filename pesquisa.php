<?php 
	include 'head.php';
?>
	<title>Smell of Love | <?php echo $lang["Pesquisa"]; ?></title>
</head>
<body>
	<div class="container-fill">
		<?php
			setarHeader();
			if (isset($_GET['pesquisa'])) {
				$pesquisa = corrigir($_GET['pesquisa']);
			} else {
				echo "<script>window.location.href = 'index.php'</script>";
			}
		?>
		<section>
			<div class="col-12 col-md-8 offset-md-2">
				<div class="row">
					<div class="col-12 mt-3">
						<h4 class="lead fonte-normal"><?php echo $lang["Resultados da busca para"]; ?>: <?php echo $pesquisa; ?></h4>
					</div>
				</div>
				<div class="row resultados-busca">
					<?php
						$sql = "SELECT * FROM produtos WHERE nome LIKE '%$pesquisa%' OR {$lang["descricao"]} LIKE '%$pesquisa%'";

						$res = $con->query($sql);

						if ($res) {
							if ($res->num_rows < 1) {
								echo "
									<p class='lead d-block mx-auto fonte-normal my-2'>
										{$lang["Nenhum Resultado Encontrado"]}!
									</p>";
							}
							while ($linha = $res->fetch_assoc()) {
					?>
								<div class="col-12 col-sm-6 px-0">
									<div class="card m-2 rounded-0 card-masculino">
								 		<img class="card-img-top w-100 img-fluid rounded-0" src="imagem.php?cod=<?php echo $linha['id']; ?>" alt="Card image cap">
										<div class="card-header border-0">
								    		<h4 class="card-title my-1 item-produto d-inline" onclick="acessarProduto('<?php echo $linha['id']; ?>')"><?php echo $linha['nome']; ?></h4>
								    		<br>
								    		<small class="fonte-normal text-center"><?php echo $lang["Conteúdo"]; ?>: <?php echo $linha['conteudo']; ?>ml</small>
								    	</div>
										<div class="card-block">						    	
								    		<p class="card-text lead fonte-normal text-justify" style="text-indent: 0">
								    			<?php echo $linha['descricao']; ?>
								    		</p>
								   		</div>
								   		<div class="card-footer border-0">
								   			<p class="card-text lead fonte-normal">
								   				R$<?php echo $linha['preco']; ?>
								   				<button value="<?php echo $linha['id']; ?>" class="btn btn-padrao float-right rounded-0" onclick="adicionarCarrinho(this)"><?php echo $lang["Adicionar ao carrinho"]; ?></button>
								   			</p>
								   		</div>
								  	</div>
						  		</div>
					<?php
							}
						}
						mysqli_close($con);
					?>
				</div>
			</div>
		</section>
		<?php include 'footer.php'; ?>
</html>